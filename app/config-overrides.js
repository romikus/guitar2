module.exports = (config) => {
  const {options} = config.module.rules[2].oneOf[1]
  options.presets.push('mobx')
  options.plugins.push([
    'transform-imports',
    {
      '@material-ui/core': {
        'transform': '@material-ui/core/esm/${member}',
        'preventFullImport': true
      },
      '@material-ui/icons': {
        'transform': '@material-ui/icons/esm/${member}',
        'preventFullImport': true
      },
      '@material-ui/lab': {
        'transform': '@material-ui/icons/esm/${member}',
        'preventFullImport': true
      }
    }
  ])

  config.resolve.alias.react = config.resolve.alias['react-dom'] = 'preact/compat'
  return config
}
