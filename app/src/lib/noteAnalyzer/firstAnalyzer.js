import {wrapChecksForDebug} from './utils'

export default class FirstAnalyzer {
  noteX2Analyzer
  noteX1n5Analyzer
  noteD2Analyzer

  constructor({
    name,
    id,
    note,
    ringBuffers,
    group,
    minWidth,
    neighbourMaxWidth,
    sampleRate,
    debug,
    ...params
  }) {
    this.name = name
    this.id = id
    this.note = note
    this.ringBuffers = ringBuffers
    this.group = group
    this.minWidth = minWidth
    this.neighbourMaxWidth = neighbourMaxWidth
    this.sampleRate = sampleRate
    Object.assign(this, params)

    this.checks = [
      checkHasFirst,
      checkHit,
      checkThatMaxGreaterThanMin,
      checkHigherThanEdgesInBetween,
      checkHigherNoteToMissItems,
      checkHigherStartedBefore,
      checkOddAndEvenEdgesDiff,
      checkLowerX2NoteNotHaveMissingOfCurrentNote,
      checkToHaveLowNoteBeforeStartIfLastMuchLowerThanFirst,
      checkToHaveItemsOnOtherSide,
    ]
    this.checksLength = this.checks.length
    if (debug) wrapChecksForDebug(debug, this.checks)
  }

  check(item, buffer, bufferIndex, time) {
    this.time = time
    const {checks, checksLength} = this
    for (let i = 0; i < checksLength; i++)
      if (!checks[i](this, item, buffer, bufferIndex))
        return false
    return true
  }
}

const checkHasFirst = (analyzer, item) => {
  if (!analyzer.groupNotHavingFirstEnabled) return true
  return !analyzer.group.hasFirst
}

const checkHit = (analyzer, item) => {
  if (!analyzer.hitCheckEnabled) return true
  return item.hit >= analyzer.firstNoteNumber && item.hit <= 7
}

const checkThatMaxGreaterThanMin = (analyzer, item, buffer) => {
  if (!analyzer.maxGreaterThanMinEnabled) return true
  const {length} = buffer
  let min = item.min
  let max = item.max
  const firstPrevPos = item.firstPrev.pos
  const {firstNoteNumber, neighbourMaxWidth, minWidth} = analyzer
  for (let i = 0; i < length; i++) {
    const el = buffer.get(i)
    if (!el || el.hit >= firstNoteNumber || el.pos < firstPrevPos - neighbourMaxWidth) break

    if (el.pos <= firstPrevPos - minWidth) {
      if (max < el.max) max = el.max
      else if (min > el.min) min = el.min
    }
  }
  return max - min >= analyzer.maxGreaterThanMinFactor
}

const checkHigherThanEdgesInBetween = (analyzer, item, buffer) => {
  if (!analyzer.higherThanEdgesInBetweenEnabled) return true
  const {length} = buffer
  let prev = item
  const {higherThanEdgesInBetweenFactor: lowerFactor} = analyzer
  while (true) {
    for (let i = 0; i < length; i++) {
      let middle = buffer.get(i)
      if (!middle || middle.pos <= prev.prev.pos) break
      if (middle.pos >= prev.pos || !middle.prev) continue

      if (prev.edge * lowerFactor < middle.edge)
        return

      let middleSum = 0
      let sum = 0
      let count = 0
      while (true) {
        middleSum += middle.edge
        sum += prev.prev.edge
        count++
        if (prev.hit === 1 || middle.hit === 0) break
        middle = middle.prev
        prev = prev.prev
      }

      break
    }
    if (prev.hit <= 2) break
    prev = prev.prev
  }
  return true
}

const checkHigherNoteToMissItems = (analyzer, item, buffer, bufferIndex) => {
  if (!analyzer.higherNoteMissItemsEnabled) return true
  return (
    checkHigherNoteToMissItemsSpecified(
      analyzer, item, buffer, bufferIndex, analyzer.noteX2Analyzer, analyzer.higherNoteMissItemsX2MinCount
    ) &&
    checkHigherNoteToMissItemsSpecified(
      analyzer, item, buffer, bufferIndex, analyzer.noteX1n5Analyzer, 3//analyzer.higherNoteMissItemsX1n5MinCount
    )
  )
}

// TODO: update params
const checkHigherNoteToMissItemsSpecified = (analyzer, item, {length}, bufferIndex, otherAnalyzer, threshold) => {
  if (!otherAnalyzer) return true

  const {firstNoteNumber} = analyzer
  let higherRingBuffer = otherAnalyzer.ringBuffers[bufferIndex]
  let count = 0
  let previous = item

  while (true) {
    for (let i = 0; i < length; i++) {
      const el = higherRingBuffer.get(i)
      if (!el) {
        count++
        break
      }

      if (el.pos === previous.pos) {
        const neighbour = el.prev || el.next
        if (neighbour) {
          if (neighbour.hit === firstNoteNumber && !neighbour.first)
            return false
        } else {
          count++
        }
        break
      }
    }

    if (previous.hit === 0) break
    previous = previous.prev
  }

  if (count >= threshold) return true

  higherRingBuffer = otherAnalyzer.ringBuffers[~bufferIndex + 2]
  count = 0
  const minPos = item.firstPrev.pos
  for (let i = 0; i < length; i++) {
    const el = higherRingBuffer.get(i)
    if (!el || el.pos < minPos) break
    if (el.prev || el.next) count++
  }

  return count <= 3
}

// TODO: update do
const checkHigherStartedBefore = (analyzer, item, {length}, bufferIndex) => {
  const negative = ~bufferIndex + 2
  return (
    checkHigherStartedBeforeSpecified(
      analyzer, item, length, bufferIndex, analyzer.noteX2Analyzer
    ) && checkHigherStartedBeforeSpecified(
      analyzer, item, length, negative, analyzer.noteX2Analyzer
    ) && checkHigherStartedBeforeSpecified(
      analyzer, item, length, bufferIndex, analyzer.noteX3Analyzer
    ) && checkHigherStartedBeforeSpecified(
      analyzer, item, length, negative, analyzer.noteX3Analyzer
    )
  )
}

const checkHigherStartedBeforeSpecified = (analyzer, item, length, bufferIndex, otherAnalyzer) => {
  if (!otherAnalyzer) return true
  const buffer = otherAnalyzer.ringBuffers[bufferIndex]
  const {pos} = item.firstPrev
  for (let i = 0; i < length; i++) {
    const el = buffer.get(i)
    if (!el || el.pos < pos) break
    if (el.first) return false
  }
  return true
}

const checkOddAndEvenEdgesDiff = (analyzer, item) => {
  if (!analyzer.oddAndEvenEdgesDiffEnabled) return true
  if (!analyzer.noteD2Analyzer) return true

  const {oddAndEvenEdgesDiffFactor: factor} = analyzer
  let sumOdd = 0
  let sumEven = 0
  let prev = item
  let count = 0
  while (true) {
    count++
    if (count % 2) sumOdd += prev.edge
    else sumEven += prev.edge
    if (prev.hit === 0) break
    prev = prev.prev
  }

  return sumOdd * factor > sumEven && sumEven * factor > sumOdd
}

const checkLowerX2NoteNotHaveMissingOfCurrentNote = (analyzer, item, buffer, bufferIndex) => {
  if (!analyzer.lowerX2NoteNoteHaveMissingsOfCurrentNoteEnabled) return true
  if (!analyzer.noteD2Analyzer) return true

  let d2Item = analyzer.noteD2Analyzer.ringBuffers[bufferIndex].get(0)
  if (!d2Item || item.prev.pos !== d2Item.pos) return true

  const {length} = buffer
  let count = 0
  while (d2Item) {
    for (let i = 0; i < length; i++) {
      const el = buffer.get(i)
      if (!el || el.pos < d2Item.pos) {
        count++
        break
      }
      if (el.pos === d2Item.pos) {
        if (!el.prev && !el.next) count++
        break
      }
    }

    d2Item = d2Item.prev
  }

  return count < analyzer.lowerX2NoteNoteHaveMissingsOfCurrentNoteMaxCount
}

const checkToHaveLowNoteBeforeStartIfLastMuchLowerThanFirst = (analyzer, item, buffer) => {
  if (!analyzer.haveLowNoteBeforeStartIfLastMuchLowerThanFirstEnabled) return false
  const {
    haveLowNoteBeforeStartIfLastMuchLowerThanFirstMaxLowerFactor: factor
  } = analyzer
  const {length} = buffer

  if (item.edge * factor < item.firstPrev.edge) {
    let count = 0
    const {
      haveLowNoteBeforeStartIfLastMuchLowerThanFirstMaxCount: maxCount
    } = analyzer
    for (let i = 0; i < length, count < maxCount; i++) {
      const el = buffer.get(i)
      if (!el) break

      if (el.pos < item.firstPrev.pos) {
        if (item.edge * factor < el.edge) {
          count++
        } else {
          break
        }
      }
    }
    if (count === maxCount)
      return
  }

  return true
}

// TODO: move params
const checkToHaveItemsOnOtherSide = (analyzer, item, _, bufferIndex) => {
  if (!analyzer.haveItemsOnOtherSideEnabled) return

  const buffer = analyzer.ringBuffers[~bufferIndex + 2]
  const {length} = buffer
  const firstPos = item.firstPrev.pos
  const {pos} = item
  let count = 0
  for (let i = 0; i < length; i++) {
    const el = buffer.get(i)
    if (!el || el.pos < firstPos) break
    if (!el.prev && !el.next || el.pos > pos) continue
    if (el.hit >= 10) return false
    count++
  }

  return count >= analyzer.haveItemsOnOtherSideMinCount
}
