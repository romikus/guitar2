import {wrapChecksForDebug} from './utils'

export default class GroupAnalyzer {
  constructor({
    note,
    noteWidth,
    minWidth,
    maxWidth,
    secondMinWidth,
    neighbourMaxWidth,
    onOpen,
    name,
    id,
    sampleRate,
    lastFirstDistanceS,
    groupIgnoreMissingCount,
    debug,
    ...params
  }) {
    this.note = note
    this.lastFirstThreshold = ~~(sampleRate * lastFirstDistanceS)
    this.lastFirstPos = -this.lastFirstThreshold
    this.hasFirst = false
    this.onOpen = onOpen
    this.minWidth = minWidth
    this.maxWidth = maxWidth
    this.secondMinWidth = secondMinWidth
    this.neighbourMaxWidth = neighbourMaxWidth
    this.lastItemPosThreshold = groupIgnoreMissingCount * noteWidth + maxWidth
    this.lastItemTailPosThreshold = 2 * noteWidth + maxWidth
    this.name = name
    this.id = id
    this.sampleRate = sampleRate
    Object.assign(this, params)

    this.checks = [
      checkLastFirstTooClose,
      checkLastIsClose,
      checkPrevNotTooLow,
      checkMinIsLowerEnoughThanAvg,
      checkOneOfThePreviousNotTooLowerThanCurrent,
      checkEdgeAfterGoingDown,
    ]
    this.checksLength = this.checks.length

    if (debug) wrapChecksForDebug(debug, this.checks, {
      invert: this.checks.slice(1)
    })
  }

  check(item, buffer, lastItem, time) {
    this.time = time
    if (!this.hasFirst) return

    const {checks, checksLength} = this
    if (checks[0](this, item)) return

    let i
    for (i = 1; i < checksLength; i++)
      if (!checks[i](this, item, buffer, lastItem))
        break
    if (i === checksLength) return

    this.hasFirst = false
    this.onOpen(this.note, item.pos, lastItem)
  }

  processItem(item) {
    if (item.first) {
      this.hasFirst = true
      this.lastFirstPos = item.pos
    }
  }
}

const checkLastFirstTooClose = (analyzer, item) => {
  if (!analyzer.lastFirstDistanceEnabled) return false
  return item.pos - analyzer.lastFirstPos <= analyzer.lastFirstThreshold
}

// TODO: update doc
const checkLastIsClose = (analyzer, item, buffer, lastItem) => {
  if (!analyzer.groupIgnoreMissingEnabled) return true
  if (item.hit !== 0) return true

  const width = item.pos - lastItem.pos
  if (width < analyzer.lastItemPosThreshold)
    return true
  if (lastItem.hit >= 10 && width < analyzer.lastItemTailPosThreshold)
    return true
  return false
}

// TODO: update doc
const checkPrevNotTooLow = (analyzer, item, buffer) => {
  if (!analyzer.prevLowerThanCurrentEnabled) return true
  const {edge} = item
  return true
  const prevNotLower = !item.prev || item.prev.edge * analyzer.prevLowerThanCurrentFactor >= edge
  if (!prevNotLower) {
    const {length} = buffer
    const {pos} = item.prev
    for (let i = 0; i < length; i++) {
      const el = buffer.get(i)
      if (!el || el.pos < pos) break
      if (el.edge > edge) return true
    }
  }
  return prevNotLower
}

const checkMinIsLowerEnoughThanAvg = (analyzer, item) => {
  if (!analyzer.minLowerThanAvgEnabled) return true
  if (item.prev && item.hit === analyzer.firstNoteNumber) {
    let min, max, sum
    min = max = sum = item.edge
    while (true) {
      if (min > item.edge) min = item.edge
      else if (max < item.edge) max = item.edge
      if (item.hit === 0) break
      item = item.prev
      sum += item.edge
    }

    if (min * analyzer.minLowerThanAvgFactor < sum / analyzer.firstNoteNumber + 1)
      return false
  }
  return true
}

const checkOneOfThePreviousNotTooLowerThanCurrent = (analyzer, {pos, edge}, buffer) => {
  if (!analyzer.oldPreviousLowerThanCurrentEnabled) return true
  const {minWidth, neighbourMaxWidth} = analyzer
  const {length} = buffer
  let position = pos
  let prevEdge = edge
  let count = 0
  const {
    oldPreviousLowerThanCurrentMaxCount: maxCount,
    oldPreviousLowerThanCurrentMinCount: minCount,
    oldPreviousLowerThanCurrentPrevHighFactor: prevHighFactor,
    oldPreviousLowerThanCurrentLowerThanCurrentFactor: lowerThanCurrentFactor,
  } = analyzer
  for (let i = 0; i < length, count <= maxCount; i++) {
    let prevItem = buffer.get(i)
    if (!prevItem) break

    const width = position - prevItem.pos
    if (width > neighbourMaxWidth || prevEdge * prevHighFactor < prevItem.edge) break
    if (width >= minWidth) {
      position = prevItem.pos
      prevEdge = prevItem.edge
      count++

      if (count > minCount && prevEdge * lowerThanCurrentFactor < edge)
        return false
    }
  }
  return true
}

const checkEdgeAfterGoingDown = (analyzer, item, buffer) => {
  if (!analyzer.descendingSequenceBeforeEnabled) return true
  const {length} = buffer

  const {maxWidth, secondMinWidth} = analyzer
  const {pos, edge} = item
  const {
    descendingSequenceBeforePrevLowerFactor: prevLowerFactor,
    descendingSequenceBeforeDescendingMinCount: descendingMinCount
  } = analyzer
  for (let i = 1; i < length; i++) {
    let prev = buffer.get(i)
    if (!prev) return true

    const width = pos - prev.pos
    if (width > secondMinWidth) break
    if (width < maxWidth || prev.hit === 0 || prev.edge * prevLowerFactor > edge) continue

    let count = 0
    let el = prev.prev
    // let limit = 3
    while (true) {
      // if (--limit === 0) break
      if (prev.edge > el.edge) break
      count++
      if (count >= descendingMinCount)
        return false
      if (el.hit === 0) break
      prev = el
      el = el.prev
    }
  }

  return true
}
