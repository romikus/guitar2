export const noop = () => {}

export const consoleLog = console.log

export const wrapChecksForDebug = (debug, checks, {invert} = {}) => {
  const {from, to, notes} = debug

  const invertHash = {}
  if (invert)
    for (let item of invert)
      invertHash[item.name] = true

  for (let i = 0; i < checks.length; i++) {
    const check = checks[i]
    const {name} = check
    const forName = {
      [name]: (analyzer, item, arg1, arg2) => {
        const between = notes[analyzer.note] && item.pos >= from && item.pos <= to
        if (between)
          console.log = consoleLog
        const result = check(analyzer, item, arg1, arg2)
        if (between) {
          item[name] = invertHash[name] ? !result : result
          console.log = noop
        }
        return result
      }
    }
    checks[i] = forName[name]
  }
}
