export const fretsCount = 19
const A4 = 440

const createString = (magicNumber) => {
  const string = []
  for (let i = 0; i <= fretsCount; i++, magicNumber--)
    string[i] = A4 / Math.pow(2, magicNumber / 12)
  return string
}

const notes = [5, 10, 14, 19, 24, 29].map(magicNumber => createString(magicNumber))
export default notes

export const notesFromTop = [...notes].reverse()

export const quartytoneRatio = Math.pow(2, 5 / 120)
export const neighbourNoteRatio = Math.pow(2, 15 / 120)

export const uniqNotes = []
notes.forEach(string =>
  string.forEach(note =>
    !uniqNotes.includes(note) && uniqNotes.push(note)
  )
)
uniqNotes.sort((a, b) => a < b ? 1 : -1)
