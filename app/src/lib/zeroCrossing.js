const decibelToFloat = (db) =>
  -Math.pow(10, db / 20)

class ZeroCrossing {
  constructor(lowestFrequency, hysteresisDb) {
    this.thresholdMin = 0.5 * decibelToFloat(hysteresisDb)
    this.thresholdMax = -this.thresholdMin
    this.up = false
    this.frame = this.maxFrame = this.minFrame = this.min = this.max = 0
  }

  push(value) {
    if (value > this.thresholdMax) {
      if (!this.up)
        this.crossUp()
      this.setMax(value)
    } else if (value < this.thresholdMin) {
      if (this.up)
        this.crossDown()
      this.setMin(value)
    }
    return this.frame++
  }

  crossUp() {
    this.up = true
    if (this.min <= this.thresholdMin)
      this.onEdge(this.minFrame, -this.min, 1)
    this.min = 0
    this.minFrame = -1
  }

  crossDown() {
    this.up = false
    if (this.max >= this.thresholdMax)
      this.onEdge(this.maxFrame, this.max, 0)
    this.max = 0
    this.maxFrame = -1
  }

  setMax(value) {
    if (this.max < value) {
      this.max = value
      this.maxFrame = this.frame
    }
  }

  setMin(value) {
    if (this.min > value) {
      this.min = value
      this.minFrame = this.frame
    }
  }
};

export default (lowestFrequency, hysteresisDb, callback) => {
  const instance = new ZeroCrossing(lowestFrequency, hysteresisDb)
  instance.onEdge = callback
  return (value) => instance.push(value)
}
