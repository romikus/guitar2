import RingBuffer from './ringBuffer'
import {quartytoneRatio, neighbourNoteRatio} from './notes'
import groupAnalyzer from './noteAnalyzer/groupAnalyzer'
import firstAnalyzer from './noteAnalyzer/firstAnalyzer'
import {noop, consoleLog} from './noteAnalyzer/utils'

export default class NoteAnalyzer {
  constructor({
    sample,
    note,
    sampleRate,
    bufferSize,
    firstNoteNumber,
    onHit,
    onGroupOpen,
    minHitEdge,
    currentAndPrevNotDiffEnabled,
    currentAndPrevNotDiffFactor,
    debug,
    ...params
  }) {
    this.name = sample.name
    this.id = ~~note
    this.note = note

    this.sampleRate = sampleRate
    this.ringBuffers = [new RingBuffer(bufferSize), new RingBuffer(bufferSize)]
    this.length = bufferSize
    this.onHit = onHit
    this.onGroupOpen = onGroupOpen
    this.firstNoteNumber = firstNoteNumber
    this.minHitEdge = minHitEdge
    this.currentAndPrevNotDiffEnabled = currentAndPrevNotDiffEnabled
    this.currentAndPrevNotDiffFactor = currentAndPrevNotDiffFactor
    this.noteWidth = ~~(sampleRate / note)
    this.lastThreshold = ~~(sampleRate * params.lastPosDistanceS)
    this.minWidth = ~~(sampleRate / (note * quartytoneRatio))
    this.secondMinWidth = this.minWidth + this.noteWidth
    this.secondMaxWidth = this.maxWidth + this.noteWidth
    this.maxWidth = ~~(sampleRate / (note / quartytoneRatio))
    this.neighbourMinWidth = ~~(sampleRate / (note * neighbourNoteRatio))
    this.neighbourMaxWidth = ~~(sampleRate / (note / neighbourNoteRatio))
    this.lastItem = {pos: -this.lastThreshold}
    this.params = params
    this.debug = debug
    this.group = this.createGroupAnalyzer()
    this.firstAnalyzer = this.createFirstAnalyzer()
  }

  createGroupAnalyzer() {
    return new groupAnalyzer({
      note: this.note,
      ringBuffers: this.ringBuffers,
      minWidth: this.minWidth,
      neighbourMaxWidth: this.neighbourMaxWidth,
      noteWidth: this.noteWidth,
      maxWidth: this.maxWidth,
      secondMinWidth: this.secondMinWidth,
      onOpen: this.onGroupOpen,
      name: this.name,
      id: this.id,
      sampleRate: this.sampleRate,
      debug: this.debug,
      ...this.params,
    })
  }

  createFirstAnalyzer() {
    return new firstAnalyzer({
      name: this.name,
      id: this.id,
      note: this.note,
      ringBuffers: this.ringBuffers,
      group: this.group,
      firstNoteNumber: this.firstNoteNumber,
      minWidth: this.minWidth,
      neighbourMaxWidth: this.neighbourMaxWidth,
      sampleRate: this.sampleRate,
      debug: this.debug,
      ...this.params,
    })
  }

  setAnalyzers(analyzers) {
    const noteX3 = this.note * 3
    const noteX2 = this.note * 2
    const noteX1n5 = this.note * 1.5
    const noteD2 = this.note / 2
    const first = this.firstAnalyzer
    for (let key in analyzers) {
      if (Math.abs(noteX3 - key) <= 1)
        first.noteX3Analyzer = analyzers[key]
      else if (Math.abs(noteX2 - key) <= 1)
        first.noteX2Analyzer = analyzers[key]
      else if (Math.abs(noteX1n5 - key) <= 1)
        first.noteX1n5Analyzer = analyzers[key]
      else if (Math.abs(key - noteD2) <= 1)
        first.noteD2Analyzer = analyzers[key]
      if (first.noteX3Analyzer && first.noteX2Analyzer && first.noteX1n5Analyzer && first.noteD2Analyzer)
        break
    }
  }

  start() {
    console.log = noop
  }

  stop() {
    console.log = consoleLog
  }

  addEdge(pos, edge, bufferIndex) {
    const time = pos / this.sampleRate

    const buffer = this.ringBuffers[bufferIndex]
    let prev, index = this.findPrevPitch(pos, buffer)
    if (index !== -1)
      prev = buffer.get(index)

    const item = this.createItem(pos, edge, bufferIndex, prev)
    this.group.check(item, buffer, this.lastItem, time)
    this.setFirst(item, buffer, bufferIndex, time)
    if (item.hit) this.lastItem = item
    this.reportHit(item)
    buffer.push(item)

    this.group.processItem(item)
  }

  findPrevPitch(pos, buffer) {
    const {minWidth, maxWidth} = this
    const {length} = buffer
    for (let i = 0; i < length; i++) {
      const item = buffer.get(i)
      if (!item) break

      const width = pos - item.pos
      if (width >= minWidth && width <= maxWidth)
        return i
    }
    return -1
  }

  createItem(pos, edge, down, prev) {
    const {
      // minHitEdge,
      currentAndPrevNotDiffEnabled: enabled,
      currentAndPrevNotDiffFactor: factor
    } = this
    let hit
    const minHitEdge = 0.05

    if (
      prev && prev.edge >= minHitEdge && edge >= minHitEdge && (
        !enabled || edge * factor >= prev.edge && prev.edge * factor >= edge
      )
    ) {
      hit = prev.hit + 1
    } else {
      hit = 0
      if (this.name === 'A16' && this.id === 277 && pos / this.sampleRate > 2.522 && pos / this.sampleRate < 2.523) {
        const buffer = this.ringBuffers[down]
        const {length} = buffer
        const minWidth = this.secondMinWidth
        const maxWidth = this.secondMaxWidth
        for (let i = 0; i < length; i++) {
          const el = buffer.get(i)
          if (!el) break
          const width = pos - el.pos
          if (width > maxWidth) break
          if (width < minWidth) continue
          if (el.hit >= 3) {
            hit = el.hit + 2
            prev = el
            break
          }
        }
      }
    }

    const min = hit === 0 ? edge : prev.min < edge ? prev.min : edge
    const max = hit === 0 ? edge : prev.max > edge ? prev.max : edge
    let item = {
      pos, edge, down, hit, prev, min, max,
      note: this.note,
      first: false,
      next: null,
      hadFirst: false,
    }
    item.firstPrev = hit === 0 ? item : prev.firstPrev
    if (prev) prev.next = item
    return item
  }

  setFirst(item, buffer, bufferIndex, time) {
    item.first = this.firstAnalyzer.check(item, buffer, bufferIndex, time)
    item.hadFirst = item.first || item.prev ? item.prev.hadFirst : false
  }

  reportHit(item) {
    if (!item.prev) return
    if (item.prev.hit === 0) this.onHit(this.note, item.prev)
    this.onHit(this.note, item)
  }
}
