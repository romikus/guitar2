import {observable} from 'mobx'

export default {
  data: observable({}),

  id(note) {
    return `noteColor:${note}`
  },

  get(note) {
    return this.data[note] || (this.data[note] = localStorage.getItem(this.id(note)))
  },

  set(note, color) {
    localStorage.setItem(this.id(note), (this.data[note] = color))
  }
}
