import {observable, action, computed, toJS} from 'mobx'
import task from '../lib/task'
import db from '../lib/db'
import Expected from './Expected'
import {remove} from './actions'

class Sample {
  @observable static records = []
  static store = db.store('samples')

  @observable id
  @observable name
  @observable.ref arrayBuffer
  @observable enablePitchSelection
  @observable expected
  @observable open
  @observable menuOpen
  @observable tab
  @observable scaleX
  @observable offsetX
  @observable analyzerOptions
  @observable.ref analyzerResult
  @observable.ref zeroCrossing
  @observable drawDetected
  @observable visualizer
  @observable debugSelection
  @observable decodedAudioData
  audioContext = new AudioContext()

  constructor({
    id, name, arrayBuffer, enablePitchSelection,
    open = true, menuOpen = false, tab = 0, scaleX = 1, offsetX = 0, expected = [],
    analyzerOptions = {},
    drawDetected = {notes: {}, notesForced: {}, colors: {}},
    visualizer = {showRuler: true, showNotes: true, showGroups: true},
    debugSelection = {enabled: false},
  }) {
    if (id)
      this.id = id
    this.name = name
    this.arrayBuffer = arrayBuffer
    this.enablePitchSelection = enablePitchSelection
    this.open = open
    this.menuOpen = menuOpen
    this.tab = tab
    this.scaleX = scaleX
    this.offsetX = offsetX
    this.expected = expected.map((expected) => new Expected(expected))
    this.analyzerOptions = analyzerOptions
    this.drawDetected = drawDetected
    this.visualizer = visualizer
    this.debugSelection = debugSelection
  }

  static map = (records) => records.map((record) => new this(record))

  static toJS({
    expected, analyzerOptions, drawDetected, visualizer, debugSelection,
    // don't save following:
    id, audioContext, analyzerResult, menuOpen, zeroCrossing, decodedAudioData,
    ...result
  }) {
    result.expected = toJS(expected)
    result.analyzerOptions = toJS(analyzerOptions)
    result.drawDetected = toJS(drawDetected)
    result.visualizer = toJS(visualizer)
    result.debugSelection = toJS(debugSelection)
    return result
  }

  @action static push = (record) =>
    this.records.push(record)

  @task static create = async (params) => {
    const record = new this(params)
    record.id = await this.store.put(this.toJS({ ...record }))
    this.push(record)
  }

  @task static update = async (id, params) => {
    const record = this.records.find((record) => record.id === id)
    const value = { ...record, ...params }
    delete value.id
    await this.store.put(this.toJS(value), id)
    Object.assign(record, params)
  }

  static save = async (params) => {
    params.id ? this.update(params.id, params) : this.create(params)
  }

  static delete = async (id) => {
    remove(this.store, this.records, id)
  }

  @action static replaceRecords = (records) =>
    this.records.replace(records)

  @task.once static async fetch() {
    const records = await this.store.all()
    this.replaceRecords(this.map([records[records.length - 1]]))
  }

  @computed get detectedStarts() {
    const {analyzerResult} = this
    return (analyzerResult ? Object.keys(analyzerResult.starts) : [])
      .map(note => +note)
      .sort((a, b) => a > b ? 1 : -1)
  }

  static get all() {
    this.fetch()
    return this.records
  }

  update(params) { Sample.update(this.id, params) }
  save() { Sample.save(this) }
  delete() { Sample.delete(this.id) }

  @action updateIndex(pos) {
    let sample = Sample.records[pos]
    if (!sample || sample === this) return
    const current = Sample.records.indexOf(this)
    let next = sample
    sample = this
    if (pos < current) {
      for (let position = pos + 1; ; position++) {
        Sample.store.put(Sample.toJS(sample), next.id)
        sample = next
        next = Sample.records[position]
        if (position > current) break
      }
    } else {
      for (let position = pos - 1; ; position--) {
        Sample.store.put(Sample.toJS(sample), next.id)
        sample = next
        next = Sample.records[position]
        if (position < current) break
      }
    }
    Sample.records.splice(current, 1)
    Sample.records.splice(pos, 0, this)
  }

  @task.once async decodeAudioData () {
    this.decodedAudioData = await this.audioContext.decodeAudioData(this.arrayBuffer.slice(0))
  }

  get audioBuffer () {
    this.decodeAudioData()
    return this.decodedAudioData
  }

  get channelData () {
    return this.audioBuffer && this.audioBuffer.getChannelData(0)
  }

  get sampleRate () {
    return this.audioBuffer && this.audioBuffer.sampleRate
  }
}

export default Sample
