import {observable, toJS} from 'mobx'
import db from 'lib/db'
import task from 'lib/task'

export default class AnalyzerParams {
  @observable static data

  // All edges
  @observable bufferSize = 32
  @observable firstNoteNumber = 5
  @observable minHitEdge = 0.04
  @observable currentAndPrevNotDiffEnabled = true
  @observable currentAndPrevNotDiffFactor = 1.8

  // Group
  @observable lastFirstDistanceEnabled = true
  @observable lastFirstDistanceS = 0.1
  @observable lastPosDistanceEnabled = true
  @observable lastPosDistanceS = 0.5
  @observable groupIgnoreMissingEnabled = true
  @observable groupIgnoreMissingCount = 1.5
  @observable prevLowerThanCurrentEnabled = true
  @observable prevLowerThanCurrentFactor = 1.7
  @observable minLowerThanAvgEnabled = true
  @observable minLowerThanAvgFactor = 1.6
  @observable oldPreviousLowerThanCurrentEnabled = true
  @observable oldPreviousLowerThanCurrentMaxCount = 5
  @observable oldPreviousLowerThanCurrentMinCount = 1
  @observable oldPreviousLowerThanCurrentPrevHighFactor = 1.1
  @observable oldPreviousLowerThanCurrentLowerThanCurrentFactor = 1.7
  @observable descendingSequenceBeforeEnabled = true
  @observable descendingSequenceBeforePrevLowerFactor = 1.6
  @observable descendingSequenceBeforeDescendingMinCount = 3

  // First note
  @observable groupNotHavingFirstEnabled = true
  @observable hitCheckEnabled = true
  @observable maxGreaterThanMinEnabled = true
  @observable maxGreaterThanMinFactor = 0.03
  @observable higherThanEdgesInBetweenEnabled = true
  @observable higherThanEdgesInBetweenFactor = 1.8
  @observable higherNoteMissItemsEnabled = true
  @observable higherNoteMissItemsX2MinCount = 3
  @observable higherNoteMissItemsX1n5MinCount = 2
  @observable oddAndEvenEdgesDiffEnabled = true
  @observable oddAndEvenEdgesDiffFactor = 1.5
  @observable lowerX2NoteNoteHaveMissingsOfCurrentNoteEnabled = true
  @observable lowerX2NoteNoteHaveMissingsOfCurrentNoteMaxCount = 2
  @observable haveLowNoteBeforeStartIfLastMuchLowerThanFirstEnabled = true
  @observable haveLowNoteBeforeStartIfLastMuchLowerThanFirstMaxLowerFactor = 1.4
  @observable haveLowNoteBeforeStartIfLastMuchLowerThanFirstMaxCount = 5
  @observable haveItemsOnOtherSideEnabled = true
  @observable haveItemsOnOtherSideMinCount = 1

  static store = db.store('analyzer_params')

  @task.once static async fetch() {
    const params = await this.store.find('params')
    this.data = new AnalyzerParams(params)
  }

  constructor(props) {
    Object.assign(this, props)
  }

  static get params() {
    this.fetch()
    return this.data
  }

  update(params) {
    Object.assign(this, params)
    AnalyzerParams.store.put(toJS(this), 'params')
  }
}
