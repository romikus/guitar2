export default () => {
  return new Promise((resolve) => {
    const req = indexedDB.open('db', 2)
    req.onsuccess = (e) => resolve(e.target.result)
    req.onupgradeneeded = (e) => {
      const db = e.target.result
      db.createObjectStore('samples', {autoIncrement: true})
      db.createObjectStore('analyzer_params')
    }
  })
}
