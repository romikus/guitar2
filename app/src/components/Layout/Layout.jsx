import React from 'react'
import AppBar from '@material-ui/core/AppBar'
import Typography from '@material-ui/core/Typography'
import Toolbar from '@material-ui/core/Toolbar'
import Tabs from '@material-ui/core/Tabs'
import Tab from '@material-ui/core/Tab'
import Box from '@material-ui/core/Box'

import CreateButton from '../Sample/CreateButton'

export default class Layout extends React.PureComponent {
  componentDidMount() {
    document.addEventListener('scroll', this.handleScroll)
    document.addEventListener('rendered', this.handleRendered)
  }

  componentWillUnmount() {
    document.removeEventListener('scroll', this.handleScroll)
    document.removeEventListener('rendered', this.handleRendered)
  }

  handleScroll = () => {
    localStorage.setItem('scrollTop', document.scrollingElement.scrollTop)
  }

  handleRendered = () =>
    document.scrollingElement.scrollTop = +localStorage.getItem('scrollTop')

  render() {
    const {children} = this.props
    return <div>
      <AppBar position="relative" color="secondary">
        <Toolbar>
          <Box mr={3}>
            <Typography variant="h5">
              Audio Analizing Script Laboratory
            </Typography>
          </Box>
          <Tabs value={0}>
            <Tab label='My Samples'>My Samples</Tab>
            <Tab label='Dataset'>My Samples</Tab>
            <Tab label='Improvise'>My Samples</Tab>
          </Tabs>
        </Toolbar>
        <Box style={{position: 'absolute', right: 0}} m={2} mt={4}>
          <CreateButton/>
        </Box>
      </AppBar>
      <Box m={2}>{children}</Box>
    </div>
  }
}
