import React from 'react'
import Fab from "@material-ui/core/Fab";
import AddIcon from "@material-ui/icons/Add";
import ModalState from './Modal/state'

export default () => {
  return <React.Fragment>
    <Fab onClick={ModalState.new} color="primary">
      <AddIcon/>
    </Fab>
  </React.Fragment>
}
