import React from 'react'
import { observer } from 'mobx-react'

import SampleModel from 'models/Sample'
import AnalyzerParams from 'models/AnalyzerParams'
import Item from './Item'
import Modal from './Modal/Modal'

export default @observer
class Samples extends React.Component {
  rendered = false

  componentDidUpdate() {
    if (this.rendered || !this.params || !this.samples.length) return

    this.rendered = true
    document.dispatchEvent(
      new CustomEvent('rendered')
    )
  }

  get params() { return AnalyzerParams.params }
  get samples() { return SampleModel.all }

  render() {
    const {params, samples} = this
    return params && <React.Fragment>
      {samples.map((sample, i) =>
        <Item key={i} samples={samples} sample={sample} params={params} />
      )}
      <Modal/>
    </React.Fragment>
  }
}
