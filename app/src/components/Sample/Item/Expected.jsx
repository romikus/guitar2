import React from "react";
import Box from "@material-ui/core/Box"
import Paper from "@material-ui/core/Paper"
import Typography from "@material-ui/core/Typography"
import Grid from "@material-ui/core/Grid"
import FormControl from "@material-ui/core/FormControl"
import InputLabel from "@material-ui/core/InputLabel"
import Select from "@material-ui/core/Select"
import MenuItem from "@material-ui/core/MenuItem"
import TextField from "@material-ui/core/TextField"
import { observer } from "mobx-react";
import notes from '../../../lib/notes'

const NumberField = observer((props) =>
  <TextField {...props} value={props.expected[props.name]} type='number' inputProps={{step: 0.0001}} />
)

export default @observer
class Expected extends React.Component {
  onChange = (e) => {
    let value = e.target.value
    if (value === undefined) value = e.target.selected
    const {expected} = this.props
    expected[e.target.name] = +value
  }

  render() {
    const {expected, number} = this.props
    return <Box alignContent='center' m={2}>
      <Paper>
        <Box p={2}>
          <Typography color="textSecondary" gutterBottom>#{number + 1}</Typography>
          <Grid container justify='center' direction='row'>
            <Box display='inline-block' p={2}>
              <NumberField label='Time From' name='from' expected={expected} onChange={this.onChange} />
            </Box>
            <Box display='inline-block' p={2}>
              <NumberField label='Time To' name='to' expected={expected} onChange={this.onChange} />
            </Box>
            <Box display='inline-block' p={2}>
              <FormControl>
                <InputLabel shrink>String</InputLabel>
                <Select name='string' onChange={this.onChange} value={expected.string}>
                  {notes.map((_string, i) =>
                    <MenuItem key={i} value={i}>{i + 1}</MenuItem>
                  )}
                </Select>
              </FormControl>
            </Box>
            <Box display='inline-block' p={2}>
              <FormControl>
                <InputLabel shrink>Fret</InputLabel>
                <Select name='fret' onChange={this.onChange} value={expected.fret}>
                  {notes[5].map((_fret, i) =>
                    <MenuItem key={i} value={i}>{i}</MenuItem>
                  )}
                </Select>
              </FormControl>
            </Box>
            <Box display='inline-block' p={2}>
              <NumberField label='Frequency' name='frequency' expected={expected} onChange={this.onChange} />
            </Box>
          </Grid>
        </Box>
      </Paper>
    </Box>
  }
}
