import React from 'react'
import {observable} from "mobx";
import {observer} from "mobx-react";
import IconButton from "@material-ui/core/IconButton";
import Button from "@material-ui/core/Button";
import MaterialMenu from "@material-ui/core/Menu";
import MenuItem from "@material-ui/core/MenuItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import Dialog from "@material-ui/core/Dialog";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogActions from "@material-ui/core/DialogActions";
import MoreVert from "@material-ui/icons/MoreVert";
import PlayArrow from "@material-ui/icons/PlayArrow"
import Edit from "@material-ui/icons/Edit";
import Delete from "@material-ui/icons/Delete";
import TextField from "@material-ui/core/TextField";

import ModalState from "../Modal/state";

export default @observer
class Menu extends React.Component {
  state = { menuAnchor: null }
  @observable confirmDialogOpen = false

  setMenuAnchor = (menuAnchor) =>
    this.setState({menuAnchor})

  openMenu = () =>
    this.props.sample.menuOpen = true

  closeMenu = () =>
    this.props.sample.menuOpen = false

  isOpen = () =>
    this.state.menuAnchor && this.props.sample.menuOpen

  play = () => {
    const {decodedAudioData, audioContext} = this.props.sample
    const source = audioContext.createBufferSource()
    source.buffer = decodedAudioData
    source.connect(audioContext.destination)
    source.start()
  }

  toggleConfirmDialog = () =>
    this.confirmDialogOpen = !this.confirmDialogOpen

  edit = () => {
    const {sample} = this.props
    ModalState.edit(sample)
  }

  delete = () => {
    const {sample} = this.props
    sample.delete()
    this.toggleConfirmDialog()
  }

  getSamplePosition() {
    const {samples, sample} = this.props
    return samples.indexOf(sample) + 1
  }

  changePosition = (e) => {
    if (!e.target.value) return

    this.props.sample.updateIndex(e.target.value - 1)
  }

  render() {
    const {menuAnchor} = this.state
    return <React.Fragment>
      <IconButton buttonRef={this.setMenuAnchor} onClick={this.openMenu}>
        <MoreVert/>
      </IconButton>
      {this.isOpen() &&
      <MaterialMenu
        elevation={0}
        getContentAnchorEl={null}
        anchorOrigin={{
          vertical: 'bottom',
          horizontal: 'center',
        }}
        transformOrigin={{
          vertical: 'top',
          horizontal: 'center',
        }}
        anchorEl={menuAnchor}
        keepMounted
        open onClose={this.closeMenu}
      >
        <MenuItem onClick={this.play}>
          <ListItemIcon>
            <PlayArrow fontSize='small'/>
          </ListItemIcon>
          <ListItemText primary='Play'/>
        </MenuItem>
        <MenuItem onClick={this.edit}>
          <ListItemIcon>
            <Edit fontSize="small"/>
          </ListItemIcon>
          <ListItemText primary="Edit"/>
        </MenuItem>
        <MenuItem onClick={this.toggleConfirmDialog}>
          <ListItemIcon>
            <Delete fontSize="small"/>
          </ListItemIcon>
          <ListItemText primary="Delete"/>
        </MenuItem>
        <MenuItem>
          <ListItemIcon>
            <TextField
              inputProps={{style: {width: '32px'}}}
              type='number'
              value={this.getSamplePosition()}
              onChange={this.changePosition}
            />
          </ListItemIcon>
          <ListItemText primary='Position'/>
        </MenuItem>
      </MaterialMenu>
      }
      <Dialog
        open={this.confirmDialogOpen}
        onClose={this.toggleConfirmDialog}
      >
        <DialogTitle>Are you sure?</DialogTitle>
        <DialogActions>
          <Button autoFocus onClick={this.toggleConfirmDialog}>
            Cancel
          </Button>
          <Button onClick={this.delete} variant='contained'>
            Delete
          </Button>
        </DialogActions>
      </Dialog>
    </React.Fragment>
  }
}
