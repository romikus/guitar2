import React from "react";
import MaterialTabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import Collapse from "@material-ui/core/Collapse";
import Box from "@material-ui/core/Box";
import { observer } from "mobx-react";

import ExpectedPanel from '../Panels/Expected'
import AnalyzerResultPanel from '../Panels/AnalyzerResult'
import AnalyzerParamsPanel from '../Panels/AnalyzerParams'
import AllNotesPanel from '../Panels/AllNotes'
import DebugPanel from '../Panels/DebugPanel/DebugPanel'
import VisualizerPanel from '../Panels/VisualizerPanel'

export default @observer
class Tabs extends React.Component {
  handleTabChange = (e, value) => {
    const {sample} = this.props
    sample.update({tab: sample.tab === value ? 0 : value})
  }

  render() {
    const {sample, params} = this.props
    return <React.Fragment>
      <MaterialTabs value={sample.tab} onChange={this.handleTabChange}>
        <Tab hidden />
        <Tab label="Expected Notes" />
        <Tab label="Analyzer Result" />
        <Tab label="Analyzer Params" />
        <Tab label="All Notes" />
        <Tab label="Debug" />
        <Tab label="Visualizer" />
      </MaterialTabs>
      <Collapse in={sample.tab}>
        <Box p={3}>
          {sample.tab === 1 && <ExpectedPanel sample={sample} />}
          {sample.tab === 2 && <AnalyzerResultPanel sample={sample} />}
          {sample.tab === 3 && <AnalyzerParamsPanel params={params} />}
          {sample.tab === 4 && <AllNotesPanel sample={sample} />}
          {sample.tab === 5 && <DebugPanel sample={sample} params={params} />}
          {sample.tab === 6 && <VisualizerPanel sample={sample} />}
        </Box>
      </Collapse>
    </React.Fragment>
  }
}
