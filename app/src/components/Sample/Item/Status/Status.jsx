import React from 'react'
import {observer} from 'mobx-react'
import Skeleton from '@material-ui/lab/Skeleton'
import style from './style.module.scss'

export default @observer
class Status extends React.Component {
  status() {
    const {sample} = this.props
    if (!sample.analyzerResult) return null

    const keys = Object.keys(sample.analyzerResult.starts)
    return keys.length === 1 && sample.analyzerResult.starts[keys[0]].length === 10
  }

  render() {
    const status = this.status()

    return <div className={style.summary}>
      <Skeleton
        variant='circle'
        width={12}
        height={12}
        animation={false}
        classes={status !== null && {circle: status ? style.success : style.fail}}
      />
    </div>
  }
}
