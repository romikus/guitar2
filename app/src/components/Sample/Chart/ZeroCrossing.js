import {observe} from 'mobx'

export default class ZeroCrossing {
  constructor({sample}) {
    this.sample = sample
    this.lineWidth = 5
    this.color = 'rgba(0, 0, 0, .3)'
  }

  start(chart) {
    this.chart = chart
    this.dispose = observe(this.sample, 'zeroCrossing', this.refresh)
  }

  refresh = () =>
    this.chart.draw()

  draw() {
    if (!this.sample.zeroCrossing)
      return

    const {context, offsetX, scaleX} = this.chart
    context.strokeStyle = this.color
    context.strokeWidth = this.lineWidth
    if (!this.path || this.path.offsetX !== offsetX || this.path.scaleX !== scaleX)
      this.path = this.createPath()
    context.stroke(this.path)
  }

  createPath() {
    const {sample: {zeroCrossing: {frames}}, chart} = this
    const {offsetX, scaleX, width, height} = chart
    const path = new Path2D()
    path.offsetX = offsetX
    path.scaleX = scaleX
    const len = frames.length
    let prevX = -1
    for (let i = 0; i < len; i++) {
      const frame = frames[i]
      const x = chart.frameToX(frame)
      if (x < 0 || x === prevX) continue
      if (x > width) break

      path.moveTo(x, 0)
      path.lineTo(x, height)
      prevX = x
    }
    return path
  }
}
