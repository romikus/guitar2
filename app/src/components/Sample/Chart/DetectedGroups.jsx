import {Component} from 'chartix'
import {utils} from 'chartix'
import {observe} from 'mobx'

import NoteColor from 'models/NoteColor'

const {shallowEqual} = utils

const empty = []

export default class DetectedGroups extends Component {
  constructor({sample}) {
    super()
    this.sample = sample
    this.noteArrows = {}
    this.notePath = {}
  }

  start(chart) {
    this.chart = chart
    this.dispose = observe(this.sample.visualizer, 'showGroups', this.refresh)
  }

  refresh = () =>
    this.chart.draw()

  stop() {
    this.dispose()
  }

  draw() {
    const {sample} = this
    if (!this.sample.visualizer.showGroups) return

    const {analyzerResult} = sample
    if (!analyzerResult) return

    const {drawDetected: {notes, notesForced}} = sample
    const {starts, groups} = analyzerResult
    for (let note in notes)
      if (notes[note] && starts[note] || notesForced[note])
        this.drawGroup(+note, groups[note] || [])
  }

  drawGroup(note, group) {
    const {context, width, height, scaleX, offsetX} = this.chart
    const size = Math.min(~~(scaleX) || 1, 5)

    const args = {note, group, width, height, scaleX, offsetX, size}
    let path = this.notePath[note]
    if (!path || !shallowEqual(path.args, args)) {
      path = this.createArrowsAndPath(args)
      path.args = args
    }

    context.strokeStyle = NoteColor.get(note)
    context.stroke(this.noteArrows[note])
    context.setLineDash([size, size])
    context.stroke(path)
    context.setLineDash(empty)
  }

  createArrowsAndPath({note, group, width, height, size}) {
    const {chart} = this
    const bottom = height - chart.wave.props.bottom
    const arrows = new Path2D()
    const path = new Path2D()
    group.forEach(item => {
      const y = bottom + size
      let x = chart.frameToX(item.pos)
      this.drawArrow(arrows, width, y - 10, size, x)
      path.moveTo(x, y - size - 10)
      path.lineTo(x, y)
      x = chart.frameToX(item.lastItem.pos)
      path.lineTo(x, y)
      path.lineTo(x, y - 10)
      this.drawArrow(arrows, width, bottom - 10, -size, x)
    })
    this.noteArrows[note] = arrows
    return this.notePath[note] = path
  }

  drawArrow(path, width, bottom, size, x) {
    if (x - size > width || x + size < 0) return

    path.moveTo(x - size, bottom)
    path.lineTo(x, bottom - size)
    path.lineTo(x + size, bottom)
  }
}
