import { Wave } from 'chartix'

export default class AudioWave extends Wave {
  constructor({chart, channelData, offsetTop, offsetBottom}) {
    super({
      values: channelData,
      stretch: true,
      stretchOffset: 5,
      fill: true,
      top: offsetTop,
      bottom: offsetBottom,
      get height() {
        return chart.height - offsetBottom - offsetTop
      },
      draw({context}, path) {
        context.strokeStyle = '#ff9800'
        context.stroke(path)
        // const height = this.height
        // context.fillStyle = '#4050ad'
        // context.fill(path)
        // context.save()
        // context.fillStyle = '#306bb8'
        // context.translate(0, height / 2)
        // context.scale(1, 0.66)
        // context.translate(0, -height / 2)
        // context.fill(path)
        // context.fillStyle = '#2086c4'
        // context.translate(0, height / 2)
        // context.scale(1, 0.4)
        // context.translate(0, -height / 2)
        // context.fill(path)
        // context.restore()
      }
    })
  }
}
