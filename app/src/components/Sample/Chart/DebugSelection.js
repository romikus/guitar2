import {RangeSelector} from 'chartix'

export default class DebugSelection {
  constructor({sample, sampleRate, duration}) {
    this.sample = sample
    this.sampleRate = sampleRate
    this.duration = duration
    this.rangeSelector = new RangeSelector({interactive: true, multiple: false})
  }

  start(chart) {
    this.chart = chart
    this.rangeSelector.start(chart)
    const {from, to} = this.sample.debugSelection
    if (from) this.rangeSelector.addSelection({from: this.toValue(from), to: this.toValue(to)})
    this.rangeSelector.events.bindListener(this, 'resizeStopSelection', this.handleResizeStopSelection)
    this.rangeSelector.events.bindListener(this, 'removeSelection', this.handleRemoveSelection)
  }

  handleResizeStopSelection = ({selection: {props}}) => {
    const {sample} = this
    const {debugSelection} = sample
    debugSelection.from = this.toPos(props.from)
    debugSelection.to = this.toPos(props.to)
    sample.save()
  }

  handleRemoveSelection = (e) => {
    if (e.move) return
    const {sample} = this
    const {debugSelection} = sample
    debugSelection.from = debugSelection.to = null
    sample.save()
  }

  draw() {
    this.rangeSelector.draw()
  }

  stop() {
    this.rangeSelector.stop(this.chart)
  }

  toPos(value) {
    return ~~(value * this.sampleRate * this.duration / this.chart.width)
  }

  toValue(pos) {
    return pos * this.chart.width / (this.sampleRate * this.duration)
  }
}
