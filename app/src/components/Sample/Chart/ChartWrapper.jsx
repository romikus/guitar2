import React from 'react'
import Box from "@material-ui/core/Box";

import Chart from './Chart'

export default class ChartWrapper extends React.Component {
  ref = React.createRef()

  componentDidMount() {
    const {sample, params} = this.props
    this.chart = new Chart(sample, params, this.ref.current)
    this.chart.start()
  }

  componentWillUnmount() {
    this.chart.stop()
  }

  render() {
    return <Box ref={this.ref} p={2}/>
  }
}
