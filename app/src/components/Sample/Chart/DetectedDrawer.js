import {Component, utils} from 'chartix'
import {observe} from 'mobx'

import NoteColor from 'models/NoteColor'

export default class DetectedDrawer extends Component {
  hitsPaths = {}
  firstsPaths = {}
  takenHits
  takenNumbers
  takenFrequencies

  start(chart) {
    super.start(chart)
    this.disposers = [
      observe(this.props.sample.drawDetected.notes, this.refresh),
      observe(this.props.sample.drawDetected.notesForced, this.refresh),
      observe(NoteColor.data, this.refresh),
      observe(this.props.sample, 'analyzerResult', this.refresh),
      observe(this.props.sample.visualizer, 'showNotes', this.refresh)
    ]
  }

  refresh = () => {
    this.chart.draw()
  }

  stop() {
    this.disposers.forEach(disposer => disposer())
  }

  draw() {
    if (!this.props.sample.visualizer.showNotes) return

    const {drawDetected: {notes, notesForced}, analyzerResult} = this.props.sample
    const {starts, hits} = analyzerResult || {starts: {}, hits: {}}
    this.takenHits = {}
    this.takenNumbers = {}
    this.takenFrequencies = {}
    for (let note in notes)
      if (notes[note] && starts[note])
        this.drawNote(+note, starts[note], hits[note])

    for (let note in notesForced)
      if (notesForced[note] && (!notes[note] || !starts[note]))
        this.drawNote(+note, [], hits[note] || [])
  }

  drawNote(note, starts, hits) {
    const {chart, takenNumbers, takenFrequencies} = this
    const {width, height, offsetX, scaleX, context} = chart
    let hitsPath = this.hitsPaths[note]
    let firstPath = this.firstsPaths[note]
    const args = {width, height, offsetX, scaleX, starts, hits}
    if (!hitsPath || !utils.shallowEqual(hitsPath.args, args)) {
      this.hitsPaths[note] = hitsPath = this.createHits(args)
      this.firstsPaths[note] = firstPath = this.createFirsts(args)
      hitsPath.args = args
    }
    context.strokeStyle = NoteColor.get(note)
    context.lineWidth = 3
    context.stroke(firstPath)
    context.lineWidth = 1
    context.stroke(hitsPath)

    if (scaleX < 10) return

    context.textBaseline = 'top'
    context.font = '14px Arial'
    context.fillStyle = 'white'
    const len = hits.length
    for (let i = 0; i < len; i++) {
      const hit = hits[i]
      if (takenNumbers[hit.pos]) continue
      takenNumbers[hit.pos] = true

      const x = chart.frameToX(hit.pos)
      if (x < 0) continue

      context.fillText(hit.hit, x + 5, hit.down ? height - 56 : 5)
    }

    if (scaleX < 30) return

    for (let i = 0; i < len; i++) {
      const hit = hits[i]
      if (takenFrequencies[hit.pos]) continue
      takenFrequencies[hit.pos] = true

      const x = chart.frameToX(hit.pos)
      if (x < 0) continue

      context.fillText(hit.edge.toFixed(4).slice(1), x + 5, hit.down ? height - 36 : 20)
    }
  }

  createFirsts({starts}) {
    const {takenHits, chart, chart: {height}} = this
    const path = new Path2D()
    const len = starts.length
    for (let i = 0; i < len; i++) {
      const {pos} = starts[i]
      const taken = takenHits[pos] || 0
      takenHits[pos] = taken + 3

      const x = chart.frameToX(pos) - 0.5 + taken
      if (x < 0) continue

      path.moveTo(x, 0)
      path.lineTo(x, height)
    }
    return path
  }

  createHits({hits}) {
    const {chart, takenHits} = this
    const path = new Path2D()
    const {height, halfHeight, highFactor, lowFactor, top} = chart.wave.path

    const len = hits.length
    const highMultiplier = -halfHeight * highFactor
    const highAdd = halfHeight + top
    const lowMultiplier = halfHeight * lowFactor
    const lowAdd = halfHeight + top
    for (let i = 0; i < len; i++) {
      const hit = hits[i]
      if (hit.first) continue

      const {pos, down} = hit
      const taken = takenHits[pos] || 0
      takenHits[pos] = taken + 3

      const x = chart.frameToX(pos) - 0.5 + taken
      if (x < 0) continue

      path.moveTo(x, down ? height : 0)
      path.lineTo(x, down ? hit.edge * lowMultiplier + lowAdd : hit.edge * highMultiplier + highAdd)
    }

    return path
  }
}
