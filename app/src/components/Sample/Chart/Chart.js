import { Chart as Chartix, Axis, Zoom, Scroll } from 'chartix'

import MiddleLine from './MiddleLine'
import Wave from './Wave'
import ExpectedSelection from './ExpectedSelection'
import ZeroCrossing from './ZeroCrossing'
import DetectedRuler from './DetectedRuler'
import DetectedGroups from './DetectedGroups'
import DetectedDrawer from './DetectedDrawer'
import DebugSelection from './DebugSelection'
import {secToX} from './utils'

export default class Chart extends Chartix {
  debounceMs = 100

  constructor(sample, params, wrapper) {
    super({wrapper, offsetX: sample.offsetX, scaleX: sample.scaleX})
    this.sample = sample
    this.params = params
    this.add(new Scroll())
    this.add(new Zoom())
    this.add(new MiddleLine())
    this.addAudioRelated(sample)
  }

  async addAudioRelated(sample) {
    const audioBuffer = await sample.audioContext.decodeAudioData(sample.arrayBuffer.slice(0))
    const channelData = audioBuffer.getChannelData(0)
    const {duration, sampleRate} = audioBuffer
    this.duration = duration
    this.sampleRate = sampleRate

    const offsetTop = 6
    const offsetBottom = 30

    this.wave = new Wave({chart: this, channelData, offsetTop, offsetBottom})
    const expectedSelection = new ExpectedSelection({sample, duration})
    this.zeroCrossing = new ZeroCrossing({sample})
    const detectedDrawer = new DetectedDrawer({sample})
    const detectedGroups = new DetectedGroups({sample})
    const detectedRuler = new DetectedRuler({sample, sampleRate, duration})
    const debugSelection = new DebugSelection({sample, sampleRate, duration})

    this.add(this.zeroCrossing)
    this.add(this.wave)
    this.add(expectedSelection)
    this.add(detectedRuler)
    this.add(detectedGroups)
    this.add(detectedDrawer)
    this.add(debugSelection)
    this.add(this.createYAxis(offsetTop, offsetBottom))
    this.add(this.createXAxis(sampleRate, channelData))
    if (this.started)
      this.draw()
  }

  createYAxis(top, bottom) {
    return new Axis({
      from: 1, to: -1, side: 'left', minSize: 20, textSpace: 40, color: '#aaa',
      top, bottom,
      formatValue: value => Math.floor(value  * 10) / 10
    })
  }

  createXAxis(sampleRate, channelData) {
    return new Axis({
      from: 0, to: channelData.length, side: 'bottom', size: 80, color: '#aaa',
      formatValue: value => Math.floor(value / sampleRate * 10000) / 10000
    })
  }

  start() {
    if (this.started)
      return
    this.started = true
    this.events.bindListener(this, 'scaleXChange', this.handleChange)
    this.events.bindListener(this, 'offsetXChange', this.handleChange)
    this.items.forEach(item => item.start && item.start(this))
    this.draw()
  }

  draw() {
    return super.draw()

    if (!this.started) return
    this.context.clearRect(0, 0, this.width, this.height)
    console.log('--- draw ---')
    let sum = 0
    this.items.forEach(item => {
      if (!item.draw) return
      console.log(item.constructor.name)
      const time = performance.now()
      item.draw()
      const diff = performance.now() - time
      console.log(diff)
      sum += diff
    })
    console.log(`--- total: ${sum} ---`)
  }

  handleChange = () => {
    if (this.timeout) clearTimeout(this.timeout)
    this.timeout = setTimeout(
      () =>
        this.sample.update({
          scaleX: this.scaleX,
          offsetX: this.offsetX,
        })
      , this.debounceMs
    )
  }

  frameToX(frame) {
    return ~~(secToX(frame / this.sampleRate, this.duration, this.width) * this.scaleX - this.offsetX)
  }
}
