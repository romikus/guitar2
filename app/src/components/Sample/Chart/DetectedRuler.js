import {quartytoneRatio} from 'lib/notes'
import {secToX} from './utils'
import NoteColor from 'models/NoteColor'
import {observe} from 'mobx'

export default class DetectedRuler {
  constructor({sample, sampleRate, duration}) {
    this.sample = sample
    this.sampleRate = sampleRate
    this.duration = duration
  }

  start(chart) {
    this.chart = chart
    this.chart.events.bindListener(this, 'mousemove', this.handleMouseOver)
    this.chart.events.bindListener(this, 'mouseout', this.handleMouseOut)
    this.x = null
    this.dispose = observe(this.sample.visualizer, 'showRuler', this.refresh)
  }

  refresh = () =>
    this.chart.draw()

  stop() {
    this.dispose()
  }

  handleMouseOver = (e) => {
    this.x = e.layerX
    this.chart.draw()
  }

  handleMouseOut = (e) => {
    this.x = null
    this.chart.draw()
  }

  draw() {
    if (!this.sample.visualizer.showRuler || this.x === null) return

    const {notes, notesForced} = this.sample.drawDetected
    const {analyzerResult} = this.sample
    const starts = analyzerResult ? analyzerResult.starts : {}
    for (let note in notes)
      if (starts[note] && notes[note])
        this.drawRulesForNote(+note)
    for (let note in notesForced)
      if (notesForced[note] && (!starts[note] || !notes[note]))
        this.drawRulesForNote(+note)

    const {context} = this.chart
    context.beginPath()
    this.drawLine(0)
    context.strokeStyle = '#fffb00'
    context.stroke()
  }

  drawRulesForNote(note) {
    const {context, scaleX, width} = this.chart
    const {duration} = this
    const noteWidth = secToX(1 / note, duration, width) * scaleX
    const maxWidth = secToX(1 / (note / quartytoneRatio), duration, width) * scaleX;
    const minWidth = secToX(1 / (note * quartytoneRatio), duration, width) * scaleX;

    context.strokeStyle = NoteColor.get(note)
    context.beginPath()
    this.drawLine(-noteWidth - maxWidth)
    this.drawLine(-noteWidth - minWidth)
    this.drawLine(-maxWidth + 1)
    this.drawLine(-minWidth - 1)
    this.drawLine(minWidth + 1)
    this.drawLine(maxWidth - 1)
    this.drawLine(noteWidth + minWidth)
    this.drawLine(noteWidth + maxWidth)
    context.stroke()
  }

  drawLine(x) {
    const {context} = this.chart
    context.moveTo(this.x + x, 0)
    context.lineTo(this.x + x, this.chart.height)
  }
}
