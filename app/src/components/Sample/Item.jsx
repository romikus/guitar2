import React from 'react'
import ExpansionPanel from "@material-ui/core/ExpansionPanel";
import ExpansionPanelSummary from "@material-ui/core/ExpansionPanelSummary";
import ExpansionPanelDetails from "@material-ui/core/ExpansionPanelDetails";
import Box from "@material-ui/core/Box";
import Divider from "@material-ui/core/Divider";
import ExpandMore from "@material-ui/icons/ExpandMore";
import styled from '@material-ui/core/styles/styled'
import withStyles from '@material-ui/core/styles/withStyles'

import Analyzer from 'components/Sample/Analyzer/Analyzer'
import ChartWrapper from './Chart/ChartWrapper'
import Menu from './Item/Menu'
import Tabs from './Item/Tabs'
import {observer} from 'mobx-react'
import Status from './Item/Status/Status'

const StyledExpansionPanelSummary = withStyles({
  content: {
    margin: '0 !important',
  }
})(ExpansionPanelSummary);

const StyledExpansionPanelDetails = styled(ExpansionPanelDetails)({
  display: 'block',
  padding: 0,
})

const stopPropagation = (e) => e.stopPropagation()

export default @observer
class Item extends React.Component {
  constructor(props) {
    super(props)
    const {sample, params} = props
    this.analyzer = new Analyzer({sample, params})
  }

  toggle = () => {
    const {sample} = this.props
    sample.update({open: !sample.open})
  }

  componentDidMount() {
    this.analyzer.analyze()
  }

  componentWillUnmount() {
    this.analyzer.stop()
  }

  render() {
    const {samples, sample, params} = this.props
    return <ExpansionPanel expanded={sample.open} onChange={this.toggle}>
      <StyledExpansionPanelSummary
        expandIcon={<ExpandMore/>}
      >
        <Box display='flex' alignItems='center' flexGrow={1}>
          <Status sample={sample} />
          {sample.name}
        </Box>
        <Box onClick={stopPropagation}>
          <Menu samples={samples} sample={sample}/>
        </Box>
      </StyledExpansionPanelSummary>
      <StyledExpansionPanelDetails>
        {
          sample.open &&
          <React.Fragment>
            <Divider/>
            <ChartWrapper sample={sample} params={params}/>
            <Divider/>
            <Tabs sample={sample} params={params}/>
          </React.Fragment>
        }
      </StyledExpansionPanelDetails>
    </ExpansionPanel>
  }
}
