import React from 'react'
import Dialog from '@material-ui/core/Dialog'
import DialogTitle from '@material-ui/core/DialogTitle'
import DialogContent from '@material-ui/core/DialogContent'
import TextField from '@material-ui/core/TextField'
import Box from '@material-ui/core/Box'
import Button from '@material-ui/core/Button'
import DialogActions from '@material-ui/core/DialogActions'
import Divider from '@material-ui/core/Divider'
import { observable, action } from 'mobx'
import { observer } from 'mobx-react/dist/mobx-react'

import SampleModel from '../../../models/Sample'
import ModalState from './state'

export default @observer
class Modal extends React.Component {
  @observable file

  @action handleNameChange = (e) => ModalState.model.name = e.target.value

  @action handleFileChange = async (e) => {
    this.file = e.target.files[0]
    ModalState.model.arrayBuffer = await this.file.arrayBuffer()
    // this.file = file
    // let source = audioContext.createBufferSource()
    // source.buffer = audioBuffer
    // source.connect(audioContext.destination)
    // source.start(0)
  }

  submit = () => {
    SampleModel.save(ModalState.model)
    ModalState.close()
  }

  render() {
    return <Dialog open={ModalState.isOpen} onClose={ModalState.close}>
      <DialogTitle>{ModalState.model.id ? 'Update' : 'Create'} New Sample</DialogTitle>
      <DialogContent>
        <TextField
          autoFocus
          margin="dense"
          label="Sample Title"
          fullWidth
          value={ModalState.model.name}
          onChange={this.handleNameChange}
        />
        <Box mt={1} mb={1}>
          <Button variant='contained' component='label' fullWidth>
            {this.file ? this.file.name : 'Upload .wav'}
            <input type='file' hidden={true} onChange={this.handleFileChange}/>
          </Button>
        </Box>
      </DialogContent>
      <Divider />
      <DialogActions>
        <Button onClick={ModalState.close}>
          Cancel
        </Button>
        <Button variant='outlined' onClick={this.submit}>
          Save
        </Button>
      </DialogActions>
    </Dialog>
  }
}

