import React from "react";
import Grid from "@material-ui/core/Grid";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Checkbox from "@material-ui/core/Checkbox";
import { observer } from "mobx-react";

import Expected from './Expected'

export default @observer
class ExpectedPanel extends React.Component {
  handleCheckboxChange = (e) => {
    const {sample} = this.props
    sample.update({[e.target.name]: e.target.checked})
  }

  render() {
    const {sample} = this.props
    if (!sample) return null
    return <React.Fragment>
      <Grid container justify='center' direction='row'>
        <FormControlLabel
          control={
            <Checkbox
              checked={sample.enablePitchSelection}
              name='enablePitchSelection'
              onChange={this.handleCheckboxChange}
              color='primary'
            />
          }
          label="Select Pitches"
        />
      </Grid>
      {sample.expected.map((expected, i) => <Expected key={i} expected={expected} number={i} />)}
    </React.Fragment>
  }
}
