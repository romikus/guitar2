import React from 'react'
import {observer} from 'mobx-react'

import {notesFromTop} from 'lib/notes'
import NotesTable from './NotesTable/NotesTable'

export default @observer
class AnalyzerResult extends React.Component {
  getParams() {
    const {sample} = this.props
    const strings = []
    const frets = []
    const len = notesFromTop.length
    sample.detectedStarts.forEach(note => {
      for (let i = 0; i < len; i++) {
        const notes = notesFromTop[i]
        if (notes.includes(note)) {
          const num = len - i
          let stringIndex = strings.findIndex(string => string.num === num)
          let arr
          if (stringIndex === -1) {
            stringIndex = strings.length
            frets[stringIndex] = arr = []
            strings.push({num, notes})
          } else {
            arr = frets[stringIndex]
          }
          const fret = notes.indexOf(note)
          if (!arr.includes(fret)) arr.push(fret)
          return
        }
      }
    })
    const lengths = frets.map(frets => frets.length)
    const maxLength = Math.max.apply(Math, lengths)
    const longestFret = frets[lengths.indexOf(maxLength)] || []
    return {strings, frets, longestFret}
  }

  render() {
    const {sample} = this.props
    const params = this.getParams()
    const short = params.strings.length > 5

    return <NotesTable {...params} checkedKey='notes' sample={sample} short={short}/>
  }
}
