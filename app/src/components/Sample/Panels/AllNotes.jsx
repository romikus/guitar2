import React from 'react'

import {notesFromTop} from 'lib/notes'
import NotesTable from './NotesTable/NotesTable'

export default
class AnalyzerResult extends React.Component {
  getParams() {
    if (this.params) return this.params

    const strings = notesFromTop.map((string, i) => ({ num: i + 1, notes: string }))
    const frets = []
    const limit = 8
    let prev = 0
    notesFromTop.forEach((string, s) => {
      const arr = frets[s] = []
      for (let i = 0, l = limit; l && i < string.length; i++) {
        const note = string[i]
        if (note > prev) {
          arr.push(i)
          prev = note
          l--
        }
      }
    })
    const longestFret = frets[0]
    const short = true

    return this.params = {strings, longestFret, frets, short}
  }

  render() {
    const {sample} = this.props
    return <NotesTable {...this.getParams()} checkedKey='notesForced' sample={sample} />
  }
}
