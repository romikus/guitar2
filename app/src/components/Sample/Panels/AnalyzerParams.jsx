import React from 'react'
import TextField from '@material-ui/core/TextField'
import styles from './AnalyzerParams.module.css'
import {observer} from 'mobx-react'
import Checkbox from '@material-ui/core/Checkbox'

export default @observer
class AnalyzerParams extends React.Component {
  handleChange = (e) => {
    const value = +e.target.value
    const {params} = this.props
    if (value)
      params.update({[e.target.name]: value})
  }

  handleCheck = (e) => {
    const {params} = this.props
    params.update({[e.target.name]: e.target.checked})
  }

  input = (name) => {
    const {params} = this.props

    return <TextField
      classes={{root: styles.field}}
      required
      type='number'
      name={name}
      value={params[name]}
      onChange={this.handleChange}
    />
  }

  check = (name) => {
    const {params} = this.props

    return <Checkbox
      color='primary'
      classes={{root: styles.checkboxRoot}}
      name={name}
      checked={params[name]}
      onChange={this.handleCheck}
    />
  }

  render() {
    const {input, check} = this

    return <div className={styles.root}>
      <h3>All hits</h3>
      <div className={styles.pad}>
        <p>
          Edges from zero crossing are stored in limited buffer with size
          {input('bufferSize')}
        </p>
        <p>
          Stable first note is
          {input('firstNoteNumber')}
          playing note in a row.
        </p>
        <p>
          Increment hit if previous note and this note edges are bigger than
          {input('minHitEdge')}
        </p>
        <p>
          {check('currentAndPrevNotDiffEnabled')}
          Increment hit current and prev are not bigger in
          {input('currentAndPrevNotDiffFactor')}
          times than another
        </p>
      </div>
      <h3>First Note Analyzer</h3>
      <div className={styles.pad}>
        <p>
          {check('groupNotHavingFirstEnabled')}
          Check that group did not have first note before
        </p>
        <p>
          {check('hitCheckEnabled')}
          Note hit should be as above in stable first note
        </p>
        <p>
          {check('maxGreaterThanMinEnabled')}
          Diff of minimum
          {input('maxGreaterThanMinFactor')}
          between max and min which are counting in sequence
          of this note and lower one till note with hit >= first note number found.
        </p>
        <p>
          {check('higherThanEdgesInBetweenEnabled')}
          Each edge in sequence must be
          {input('higherThanEdgesInBetweenFactor')}
          times bigger than edge in between current and prev.
        </p>
        <p>
          {check('higherNoteMissItemsEnabled')}
          Higher note must have missing items in parallel sequence.<br/>
          Minimal count for 2 times higher is
          {input('higherNoteMissItemsX2MinCount')}<br/>
          Minimal count for 1.5 times higher is
          {input('higherNoteMissItemsX1n5MinCount')}
        </p>
        <p>
          {check('oddAndEvenEdgesDiffEnabled')}
          Sum of odd elements in sequence must be
          {input('oddAndEvenEdgesDiffFactor')}
          times lower or higher than sum of even elements.
        </p>
        <p>
          {check('lowerX2NoteNoteHaveMissingsOfCurrentNoteEnabled')}
          Lower 2 times note must have less than
          {input('lowerX2NoteNoteHaveMissingsOfCurrentNoteMaxCount')}
          missing notes of current frequency.
        </p>
        <p>
          {check('haveLowNoteBeforeStartIfLastMuchLowerThanFirstEnabled')}
          If first item in sequence has big db and last is low,
          there must be lower items before sequence start.<br/>
          Exactly
          {input('haveLowNoteBeforeStartIfLastMuchLowerThanFirstMaxCount')}
          items with higher value before start required.<br/>
          Items before start must be not greater than
          {input('haveLowNoteBeforeStartIfLastMuchLowerThanFirstMaxLowerFactor')}
          times current.
        </p>
        <p>
          {check('haveItemsOnOtherSideEnabled')}
          Check to have at least
          {input('haveItemsOnOtherSideMinCount')}
          on the other side.
        </p>
      </div>
      <br/>
      <h3>Group Analyzer</h3>
      <div className={styles.pad}>
        <p>
          {check('lastFirstDistanceEnabled')}
          Previous first note must be more than
          {input('lastFirstDistanceS')}
          seconds before.
        </p>
        <p>
          {check('lastPosDistanceEnabled')}
          If previous any note is more than
          {input('lastPosDistanceS')}
          seconds before.
        </p>
        <p>
          {check('groupIgnoreMissingEnabled')}
          Start new group after
          {input('groupIgnoreMissingCount')}
          note widths of absent notes.
        </p>
        <p>
          {check('prevLowerThanCurrentEnabled')}
          If previous is
          {input('prevLowerThanCurrentFactor')}
          times lower than current
        </p>
        <p>
          {check('minLowerThanAvgEnabled')}
          If current has first note number and minimal edge in sequence is
          {input('minLowerThanAvgFactor')}
          times lower than average
        </p>
        <p>
          {check('oldPreviousLowerThanCurrentEnabled')}
          For the rising sequence: find minimal previous and compare to current.<br/>
          Maximum size of sequence is
          {input('oldPreviousLowerThanCurrentMaxCount')}<br/>
          Sequence can include lower neighbour note.<br/>
          Minimum items in sequence is
          {input('oldPreviousLowerThanCurrentMinCount')}<br/>
          Rising sequence means that there is no next lower than prev in
          {input('oldPreviousLowerThanCurrentPrevHighFactor')} times.<br/>
          Create new group if minimal in sequence is
          {input('oldPreviousLowerThanCurrentLowerThanCurrentFactor')}
          times lower than current.
        </p>
        <p>
          {check('descendingSequenceBeforePrevLowerEnabled')}
          Open group if there is a note before,
          and note is between `maxWidth` and `maxWidth` + `noteWidth`,<br/>
          and that note is at least
          {input('descendingSequenceBeforePrevLowerFactor')}
          times lower than current,<br/>
          and sequence of previous starting from this note have at least
          {input('descendingSequenceBeforeDescendingMinCount')}
          elements, where each is higher that other.
        </p>
      </div>
    </div>
  }
}
