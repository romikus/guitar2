import React from 'react'
import Table from '@material-ui/core/Table'
import TableBody from '@material-ui/core/TableBody'
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead'
import TableRow from '@material-ui/core/TableRow'
import Checkbox from '@material-ui/core/Checkbox';
import {observer} from 'mobx-react'

import styles from './style.module.scss'
import NoteColor from 'models/NoteColor'

@observer
class Note extends React.Component {
  toggleNote = (e) => {
    const {sample, note, checkedKey} = this.props
    sample.drawDetected[checkedKey][note] = e.target.checked
    sample.save()
  }

  setColor = (e) => {
    const {note} = this.props
    NoteColor.set(note, e.target.value)
  }

  render() {
    const {sample, index, fret, note, checkedKey} = this.props
    const {starts, hits} = sample.analyzerResult || { starts: {}, hits: {} }
    const {[checkedKey]: notes} = sample.drawDetected

    if (fret === undefined)
      return <TableCell colSpan={5} className={styles.td} />

    return <React.Fragment>
      <TableCell className={`${styles.td} ${index && styles.notFirst}`}>
        <Checkbox
          classes={{root: styles.checkboxWrap}}
          checked={notes[note]}
          onChange={this.toggleNote}
          color='primary'
        />
        <input
          value={NoteColor.get(note)}
          onChange={this.setColor}
          type='color'
          className={styles.inputColor}
        />
      </TableCell>
      <TableCell className={styles.td}>{fret}</TableCell>
      <TableCell className={styles.td}>{Math.floor(note)}</TableCell>
      <TableCell className={styles.td}>{starts[note]?.length || 0}</TableCell>
      <TableCell className={styles.td}>{hits[note]?.length || 0}</TableCell>
    </React.Fragment>
  }
}

export default class NotesTable extends React.Component {
  render() {
    const {sample, strings, frets, longestFret, short, checkedKey} = this.props

    return <Table className={short && styles.short} stickyHeader aria-label="sticky table">
      <TableHead>
        <TableRow>
          {strings.map((string, i) => (
            <React.Fragment key={string.num}>
              <TableCell className={`${styles.td} ${i && styles.notFirst}`}>Draw</TableCell>
              <TableCell className={styles.td}>{short ? string.num : `String ${string.num}`}</TableCell>
              <TableCell className={styles.td}>{short ? 'F' : 'Frequency'}</TableCell>
              <TableCell className={styles.td}>{short ? 'S' : 'Starts'}</TableCell>
              <TableCell className={styles.td}>{short ? 'H' : 'Hits'}</TableCell>
            </React.Fragment>
          ))}
        </TableRow>
      </TableHead>
      <TableBody>
        {longestFret.map((_, index) =>
          <TableRow tabIndex={-1} key={index}>
            {strings.map((string, i) => {
              const fret = frets[i][index]
              return <Note
                sample={sample}
                index={i}
                fret={fret}
                note={string.notes[fret]}
                checkedKey={checkedKey}
              />
            })}
          </TableRow>
        )}
      </TableBody>
    </Table>
  }
}
