import React from 'react'
import Checkbox from '@material-ui/core/Checkbox'
import styles from './AnalyzerParams.module.css'
import {observer} from 'mobx-react'

export default @observer
class DebugPanel extends React.Component {
  handleCheck = (e) => {
    const {sample} = this.props
    sample.visualizer[e.target.name] = e.target.checked
    sample.save()
  }

  check = (name) => {
    const {visualizer} = this.props.sample

    return <Checkbox
      color='primary'
      classes={{root: styles.checkboxRoot}}
      name={name}
      checked={visualizer[name]}
      onChange={this.handleCheck}
    />
  }

  render() {
    const {check} = this
    return <div>
      <p>{check('showRuler')} Show ruler for detected notes</p>
      <p>{check('showNotes')} Show notes</p>
      <p>{check('showGroups')} Show groups of detected notes</p>
    </div>
  }
}
