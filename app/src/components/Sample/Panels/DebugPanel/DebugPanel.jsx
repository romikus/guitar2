import React from 'react'
import Table from '@material-ui/core/Table'
import TableHead from '@material-ui/core/TableHead'
import TableBody from '@material-ui/core/TableBody'
import TableRow from '@material-ui/core/TableRow'
import TableCell from '@material-ui/core/TableCell'
import Tooltip from '@material-ui/core/Tooltip'
import CheckIcon from '@material-ui/icons/Check'
import CloseIcon from '@material-ui/icons/Close'
import {observer} from 'mobx-react'

import NoteColor from 'models/NoteColor'
import style from './style.module.css'
import {uniqNotes} from 'lib/notes'
import noteAnalyzer from 'lib/noteAnalyzer'

class DebugItem extends React.Component {
  bool = (value) =>
    value ? <CheckIcon/> : <CloseIcon/>

  render() {
    const {item, sampleRate, checks} = this.props
    const {bool} = this
    return <TableRow>
      <TableCell>{~~(item.pos * 1000 / sampleRate) / 1000}</TableCell>
      <TableCell align='center'>{bool(item.first)}</TableCell>
      <TableCell align='center'>{~~(item.edge * 1000) / 1000}</TableCell>
      <TableCell align='center'>{item.hit}</TableCell>
      {checks.map((check, i) =>
        <TableCell key={i} align='center'>
          {bool(item[check.name])}
        </TableCell>
      )}
    </TableRow>
  }
}

class DebugNote extends React.Component {
  render () {
    const {note, items, sampleRate, checks} = this.props

    return <Table stickyHeader>
      <TableHead>
        <TableRow>
          <TableCell>
            <div className={style.circle} style={{background: NoteColor.get(note)}}/>
            {~~note}
          </TableCell>
          <TableCell align='center'>First</TableCell>
          <TableCell align='center'>Edge</TableCell>
          <TableCell align='center'>Hit</TableCell>
          {checks.map((check, i) =>
            <TableCell key={i} align='center'>
              <Tooltip title={check.readable} placement='top'><div>{i + 1}</div></Tooltip>
            </TableCell>
          )}
        </TableRow>
      </TableHead>
      <TableBody>
        {items.map((item, i) =>
          <DebugItem key={i} item={item} sampleRate={sampleRate} checks={checks} />
        )}
      </TableBody>
    </Table>
  }
}

export default @observer
class DebugPanel extends React.Component {
  notes
  checks

  componentWillMount() {
    const {sample} = this.props
    sample.debugSelection.enabled = true
    sample.save()
  }

  componentWillUnmount() {
    const {sample} = this.props
    sample.debugSelection.enabled = false
    sample.save()
  }

  analyze() {
    const {sample, params} = this.props
    const {
      drawDetected: {notes, notesForced}, sampleRate, channelData, zeroCrossing, debugSelection: {from, to}
    } = sample
    if (!sampleRate || !channelData || !zeroCrossing) return false

    const showNotes = {}
    for (let note in notes)
      if (notes[note])
        showNotes[note] = true
    for (let note in notesForced)
      if (notesForced[note])
        showNotes[note] = true

    this.notes = {}
    const handleHit = this.handleHit.bind(null, from, to, this.notes, showNotes)
    const {handleGroupOpen} = this
    const analyzers = {}
    const uniqNotesLength = uniqNotes.length
    for (let i = 0; i < uniqNotesLength; i++) {
      const note = uniqNotes[i]
      analyzers[note] = new noteAnalyzer({
        debug: {from, to, notes: showNotes},
        sample, note, sampleRate, analyzers, ...params,
        onHit: handleHit,
        onGroupOpen: handleGroupOpen
      })
    }

    for (let i = 0; i < uniqNotesLength; i++) {
      const analyzer = analyzers[uniqNotes[i]]
      analyzer.setAnalyzers(analyzers)
      analyzer.start()
    }

    const {edges, frames, bufferIndices} = zeroCrossing
    const edgesLength = edges.length
    for (let j = 0; j < edgesLength; j++)
      for (let i = 0; i < uniqNotesLength; i++)
        analyzers[uniqNotes[i]].addEdge(frames[j], edges[j], bufferIndices[j])

    for (let i = 0; i < uniqNotesLength; i++)
      analyzers[uniqNotes[i]].stop()

    const first = analyzers[uniqNotes[0]]
    const checkMapper = check => ({
      name: check.name,
      readable: check.name
        .replace(/^check/, '')
        .replace(/[A-Z]/g, (letter, i) =>
          `${i ? ' ' : ''}${letter.toLowerCase()}`
        )
    })

    this.firstChecks = first.firstAnalyzer.checks.map(checkMapper)
    this.groupChecks = first.group.checks.map(checkMapper)

    return true
  }

  handleHit (from, to, notes, showNotes, note, item) {
    if (item.pos < from || item.pos > to) return
    if (!showNotes[note]) return

    const items = notes[note] || (notes[note] = [])
    items.push(item)
  }

  handleGroupOpen() {}

  render() {
    const {sample} = this.props
    if (sample.debugSelection.to === undefined) return null
    if (!this.analyze()) return null

    const {notes, firstChecks, groupChecks} = this
    const {sampleRate} = sample
    return <React.Fragment>
      <h3>First Note Checks</h3>
      {Object.keys(notes).map(note =>
        <DebugNote key={note} note={note} items={notes[note]} sampleRate={sampleRate} checks={firstChecks} />
      )}
      <br/>
      <h3>Group Checks</h3>
      {Object.keys(notes).map(note =>
        <DebugNote key={note} note={note} items={notes[note]} sampleRate={sampleRate} checks={groupChecks} />
      )}
    </React.Fragment>
  }
}
