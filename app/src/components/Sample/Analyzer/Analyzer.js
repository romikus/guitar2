import {observe} from 'mobx'

import notes, {uniqNotes, quartytoneRatio} from 'lib/notes'
import zeroCrossing from 'lib/zeroCrossing'
import noteAnalyzer from 'lib/noteAnalyzer'

export default class Analyzer {
  frames
  edges
  i
  zeroCrossing
  analyzers
  result

  constructor({sample, params}) {
    this.sample = sample
    this.params = params
  }

  async analyze() {
    const {sample} = this
    await sample.decodeAudioData()

    this.channelData = sample.channelData
    this.sampleRate = sample.sampleRate
    const length = this.channelData.length / 10
    this.frames = new Uint32Array(length)
    this.edges = new Float32Array(length)
    this.bufferIndices = new Uint8Array(length)
    this.i = 0
    this.zeroCrossing = this.createZeroCrossing()
    this.result = this.initResult()
    this.analyzers = this.createAnalyzers()
    this.dispose = this.subscribeToParamsChange()

    this.process()
    this.applyZeroCrossing()
    this.applyResult()
  }

  stop() {
    this.dispose()
  }

  process() {
    const {analyzers} = this
    for (let note in analyzers)
      analyzers[note].start()
    this.channelData.forEach(this.zeroCrossing)
    for (let note in analyzers)
      analyzers[note].stop()
  }

  applyZeroCrossing() {
    this.sample.zeroCrossing = this
  }

  applyResult() {
    this.sample.analyzerResult = this.result
  }

  subscribeToParamsChange() {
    return observe(this.params, this.handleParamsChange)
  }

  handleParamsChange = () => {
    const {frames, edges, bufferIndices} = this
    this.result = this.initResult()
    let analyzers
    this.analyzers = analyzers = this.createAnalyzers()
    uniqNotes.forEach(note => {
      const analyzer = analyzers[note]
      frames.forEach((frame, i) =>
        analyzer.addEdge(frame, edges[i], bufferIndices[i])
      )
    })
    this.applyResult()
  }

  createZeroCrossing() {
    return zeroCrossing(
      notes[0][0] / quartytoneRatio, -30, this.handleZeroCrossing
    )
  }

  initResult() {
    return { starts: {}, hits: {}, groups: {} }
  }

  createAnalyzers() {
    const {sample, sampleRate, params, handleHit, handleGroupOpen} = this
    const analyzers = {}

    uniqNotes.forEach(note =>
      analyzers[note] = new noteAnalyzer({
        sample, note, sampleRate, analyzers, ...params,
        onHit: handleHit,
        onGroupOpen: handleGroupOpen
      })
    )

    uniqNotes.forEach(note =>
      analyzers[note].setAnalyzers(analyzers)
    )

    return analyzers
  }

  handleZeroCrossing = (frame, edge, bufferIndex) => {
    this.frames[this.i] = frame
    this.edges[this.i] = edge
    this.bufferIndices[this.i] = bufferIndex
    this.i++
    const {analyzers} = this
    uniqNotes.forEach(note =>
      analyzers[note].addEdge(frame, edge, bufferIndex)
    )
  }

  handleHit = (note, item) => {
    this.addItem(this.result.hits, note, item)
    if (item.first)
      this.addItem(this.result.starts, note, item)
  }

  handleGroupOpen = (note, pos, lastItem) => {
    this.addItem(this.result.groups, note, {pos, lastItem})
  }

  addItem(object, note, item) {
    let items = object[note]
    items ? items.push(item) : (object[note] = [item])
  }
}
