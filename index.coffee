fs = require 'fs'
WavDecoder = require 'wav-decoder'
WavEncoder = require 'wav-encoder'

getTrack = (name) ->
  file = fs.readFileSync "samples/#{name}.wav"
  track = await WavDecoder.decode file
  data = track.channelData[0]
  {data, sampleRate: track.sampleRate, channelData: track.channelData}

A4 = 440

semitoneRatio = Math.pow 2, 100 / 1200
quartytoneRatio = Math.pow 2, 50 / 1200
neighbourNoteRatio = Math.pow 2, 150 / 1200

E = A4 / Math.pow 2, 29 / 12
E1 = E * semitoneRatio
E2 = E1 * semitoneRatio
E3 = E2 * semitoneRatio
E4 = E3 * semitoneRatio

sampleRate = 44100

write = (name, channelData) ->
  data = {sampleRate, channelData: [channelData]}

  encoded = await WavEncoder.encode data
  fs.writeFileSync "samples/#{name}.wav", new Buffer encoded

checkCustomZeroCrossing = (name, target) ->
  {data} = await getTrack name
  sum = 0
  sum += item for item in data

  ZeroCrossing = require './lib/custom_zero_crossing'
  zeroCrossing = new ZeroCrossing E / quartytoneRatio, sampleRate, -30

  RingBuffer = require './lib/custom_ring_buffer'
  ringBuffer = new RingBuffer 32

  from = Math.floor 2.62 * sampleRate
  to = Math.floor 2.84 * sampleRate

  from = 0
  to = Infinity

  num = 1
  out = data.slice()

  windowWidth = ~~(sampleRate / E) * 3
  lastThreshold = ~~(sampleRate * 0.05)

  noteWidth = ~~(sampleRate / target)
  maxWidth = ~~(sampleRate / (target / quartytoneRatio))
  minWidth = ~~(sampleRate / (target * quartytoneRatio))
  minNeighbourWidth = ~~(sampleRate / (target * neighbourNoteRatio))
  maxPrevWidth = ~~(sampleRate / (target / quartytoneRatio)) + noteWidth
  minPrevWidth = ~~(sampleRate / (target * quartytoneRatio)) + noteWidth
  lastPos = -lastThreshold
  lastFirstPos = -lastThreshold
  stableHitThreshold = 3
  minFirstEdge = 0.03

  zeroCrossing.onEdge = (pos, edge) ->
    if pos / sampleRate < 4.4
      return

    pos += from
    out[pos] = 0.3
    currentHit = 0

    for i in [0...ringBuffer.length]
      item = ringBuffer.get i
      break unless item

      width = pos - item.pos
      if width > windowWidth
        ringBuffer.set i, null
        break

    checkedNoteBefore = false
    previousNote = false
    for i in [0...ringBuffer.length]
      item = ringBuffer.get i
      break unless item

      width = pos - item.pos
      #      break if width > EhalfMin and width < EhalfMax

      if width <= maxWidth
        if width >= minWidth
          currentHit = item.hit + 1
          break
#        else if width >= minNeighbourWidth
#          unless checkedNoteBefore
#            for j in [i + 1...ringBuffer.length]
#              prev = ringBuffer.get j
#              break unless prev
#
#              if prev.hit
#                width = pos - prev.pos
#                if width <= maxPrevWidth and width >= minPrevWidth
#                  previousNote = prev
#                  break
#            checkedNoteBefore = true
#          if previousNote and previousNote.hit >= stableHitThreshold
#            previousNote.hit++
#            currentHit = previousNote.hit + 1
#            break

    if currentHit
      num++
      out[pos] = 1
      out[item.pos] = 1
      if item.edge > minFirstEdge
        width = item.pos - lastPos
        lastPos = item.pos
        if width >= lastThreshold
          item.first = true
        else
          if (
            item.pos - lastFirstPos >= lastThreshold and
            edge * 1.5 > item.edge and
            Math.abs(edge - item.edge) > 0.01
          )
            item.first = true
            for j in [i + 1...ringBuffer.length]
              prev = ringBuffer.get j
              break unless prev

              width = item.pos - prev.pos
              if (
                prev.hit or ((width = item.pos - prev.pos) > minNeighbourWidth and width < maxWidth)
              ) and (
                prev.edge < item.edge and prev.edge * 1.4 > item.edge or
                prev.edge > item.edge and item.edge * 2 > prev.edge
              )
                item.first = false
                break

      item.hit = currentHit
      currentHit = item.hit
      if item.first
        lastFirstPos = item.pos
        out[item.pos - 1] = -1
#    else
#      out[pos - 1 - minWidth] = -0.25
#      out[pos - 1 - maxWidth] = -0.25
#      out[pos - 1 - ~~(sampleRate / (E3 * quartytoneRatio))] = -1
#      out[pos - 1 - ~~(sampleRate / (E3 / quartytoneRatio))] = -0.75
#      out[pos - 1 - ~~(sampleRate / (E2 * Math.pow(2, 150 / 1200)))] = -0.5

    ringBuffer.push {pos, edge, first: false, hit: currentHit}

#  for s, i in data
#    if i >= from and i <= to
#      zeroCrossing.push s

  await write name + '-output', out

checkCustomZeroCrossing '1/0', E
#checkCustomZeroCrossing '1/1', E1
#checkCustomZeroCrossing '1/2', E2
#checkCustomZeroCrossing '1/3', E3
#checkCustomZeroCrossing '1/4', E4

import RingBuffer from './ring_buffer'

var A4, E, E1, E2, E3, E4, neighbourNoteRatio, quartytoneRatio, sampleRate, semitoneRatio;

A4 = 440;

semitoneRatio = Math.pow(2, 100 / 1200);

quartytoneRatio = Math.pow(2, 50 / 1200);

neighbourNoteRatio = Math.pow(2, 150 / 1200);

E = A4 / Math.pow(2, 29 / 12);

E1 = E * semitoneRatio;

E2 = E1 * semitoneRatio;

E3 = E2 * semitoneRatio;

E4 = E3 * semitoneRatio;

sampleRate = 44100;

export default class NoteAnalyzer {
  constructor({note: target, onHit, onFirst}) {
  console.log(target)
var from, lastFirstPos, lastPos, lastThreshold, maxWidth,
  minFirstEdge, minNeighbourWidth, minWidth, num, ringBuffer, windowWidth;
ringBuffer = new RingBuffer(32);
from = 0;
num = 1;
windowWidth = ~~(sampleRate / E) * 3;
lastThreshold = ~~(sampleRate * 0.05);
maxWidth = ~~(sampleRate / (target / quartytoneRatio));
minWidth = ~~(sampleRate / (target * quartytoneRatio));
minNeighbourWidth = ~~(sampleRate / (target * neighbourNoteRatio));
lastPos = -lastThreshold;
lastFirstPos = -lastThreshold;
minFirstEdge = 0.03;
this.addEdge = function (pos, edge) {
if (pos / sampleRate < 4.4) return

var currentHit, i, item, j, k, l, m, prev, ref, ref1, ref2, ref3, width;
pos += from;
currentHit = 0;

if (pos / sampleRate < 4.403)
  console.log(pos / sampleRate)

for (i = k = 0, ref = ringBuffer.length; (0 <= ref ? k < ref : k > ref); i = 0 <= ref ? ++k : --k) {
  item = ringBuffer.get(i);
if (!item) {
  break;
}
width = pos - item.pos;
if (width > windowWidth) {
  ringBuffer.set(i, null);
  break;
}
}
for (i = l = 0, ref1 = ringBuffer.length; (0 <= ref1 ? l < ref1 : l > ref1); i = 0 <= ref1 ? ++l : --l) {
  item = ringBuffer.get(i);
if (!item) {
  break;
}
width = pos - item.pos;
if (width <= maxWidth) {
  if (width >= minWidth) {
    currentHit = item.hit + 1;
    break;
}
}
}
if (currentHit) {
    onHit(item)
  num++;
  if (item.edge > minFirstEdge) {
    width = item.pos - lastPos;
    lastPos = item.pos;
    if (width >= lastThreshold) {
      item.first = true;
    } else {
    if (item.pos - lastFirstPos >= lastThreshold && edge * 1.5 > item.edge && Math.abs(edge - item.edge) > 0.01) {
      item.first = true;
      for (j = m = ref2 = i + 1, ref3 = ringBuffer.length; (ref2 <= ref3 ? m < ref3 : m > ref3); j = ref2 <= ref3 ? ++m : --m) {
        prev = ringBuffer.get(j);
      if (!prev) {
        break;
      }
      width = item.pos - prev.pos;
      if ((prev.hit || ((width = item.pos - prev.pos) > minNeighbourWidth && width < maxWidth)) && (prev.edge < item.edge && prev.edge * 1.4 > item.edge || prev.edge > item.edge && item.edge * 2 > prev.edge)) {
        item.first = false;
        break;
      }
      }
    }
    }
  }
  item.hit = currentHit;
  currentHit = item.hit;
  if (item.first) {
    lastFirstPos = item.pos;
    onFirst(item)
    console.log('first', item.pos / sampleRate)
  }
}
return ringBuffer.push({
  pos,
  edge,
  first: false,
  hit: currentHit
});
};
}
}

