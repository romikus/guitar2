Bitset = require './bitset'
RingBuffer = require './ring_buffer'

pulse_height_diff = 0.8
pulse_width_diff = 0.85

class Info
  constructor: (@prev, @current, @peak, @leading_edge) ->
    @width = 0

  updatePeak: (s, frame) ->
    if s > @peak
      @peak = s
    else if @width is 0 and s < @peak * 0.3
      @width = frame - @leading_edge

  period: (next) ->
    next.leading_edge - @leading_edge

  fractionalPeriod: (next) ->
    next.leading_edge - @leading_edge + (next.edge() - @edge())

  edge: ->
    -@prev / (@current - @prev)

  similar: (next) ->
    @relWithin(@peak, next.peak, 1 - pulse_height_diff) and
      @relWithin(@width, next.width, 1 - pulse_width_diff)

  relWithin: (a, b, eps) ->
    Math.abs(a - b) <= eps * Math.max(Math.abs(a), Math.abs(b))

module.exports = class ZeroCrossing
  constructor: (hysteresis, window) ->
# console.log "zc window: #{window}"
    @prev = 0
    @hysteresis = -Math.pow(10, hysteresis / 20)
    @info = new RingBuffer 64
    @capacity = @info.size
    @ready = false
    @peak = @peak_update = 0
    valueSize = Bitset::valueSize
    @windowSize = ~~((window + valueSize - 1) / valueSize) * valueSize
    @reset()

  reset: ->
    @num_edges = 0
    @state = false
    @frame = 0

  peakPulse: ->
    Math.max @peak, @peak_update

  is_reset: ->
    @frame is 0

  value: (s) ->
    # Offset s by half of hysteresis, so that zero cross detection is
    # centered on the actual zero.
    s += @hysteresis / 2

    if @num_edges >= @capacity
      @reset()

    if @frame is @windowSize / 2 and @num_edges is 0
      @reset()

    @update_state s

    @frame++
    if @frame >= @windowSize and not @state
      # Remove half the size from frame, so we can continue seamlessly
      @frame -= @windowSize / 2

      if @num_edges > 1
        @ready = true
      else
        @reset()

    @state

  get: (index) ->
    @info.get @num_edges - 1 - index

  update_state: (s) ->
    if @ready
      @shift @windowSize / 2
      @ready = false
      @peak = @peak_update
      @peak_update = 0

    if @num_edges >= @capacity
      @reset()

    if s > 0
      if not @state
        @info.push new Info @prev, s, s, @frame
        @num_edges++
        @state = true
      else
        @info.get(0).updatePeak s, @frame

      if s > @peak_update
        @peak_update = s

    else if @state and s < @hysteresis
      @state = false
      @info.get(0).trailing_edge = @frame

      if @peak is 0
        @peak = @peak_update

    @prev = s

  shift: (n) ->
    @info.get(0).leading_edge -= n
    unless @state
      @info.get(0).trailing_edge -= n

    i = 1
    while i isnt @num_edges
      @info.get(i).leading_edge -= n
      edge = @info.get(i).trailing_edge -= n
      if edge < 0
        break

      i++

    @num_edges = i
