module.exports = class MovingAverage
  constructor: (@count, value) ->
    @set value

  set: (@value) ->
    @a = 2 / (@count + 1)
    @b = 1 - @a

  update: (newValue) ->
    @value = @a * newValue + @b * @value

  subtract: (value) ->
    @value = (@value - @a * value) / @b
