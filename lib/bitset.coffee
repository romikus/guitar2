module.exports = class Bitset extends BigUint64Array
  allOnes: `18446744073709551615n`
  valueSize: valueSize = 8 * BigUint64Array.BYTES_PER_ELEMENT
  bigValueSize = BigInt valueSize

  constructor: (numBits) ->
    super (numBits + valueSize - 1) / valueSize
    @size = @length * valueSize

  set: (i, val) ->
    mask = `1n` << (BigInt(i) % bigValueSize)
    i = Math.floor i / valueSize
    @[i] ^= (-BigInt(val) ^ @[i]) & mask

  get: (i) ->
    mask = `1n` << (BigInt(i) % bigValueSize)
    (@[Math.floor i / valueSize] & mask) isnt `0n`

  clear: ->
    @fill `0n`

  set3: (i, n, val) ->
# Check that the n does not get past the size
    if i + n > @size
      n = @size - i

    n = BigInt n
    p = Math.floor i / valueSize

    # Do the first partial int
    mod = BigInt(i) & (bigValueSize - `1n`)
    if (mod)
# mask off the high n bits we want to set
      mod = bigValueSize - mod

      # Calculate the mask
      mask = ~(@allOnes >> mod)

      # Adjust the mask if we're not going to reach the end of this int
      if n < mod
        mask &= (@allOnes >> (mod - n))

      if val
        @[p] |= mask
      else
        @[p] &= ~mask

      # Fast exit if we're done here!
      if (n < mod)
        return

      n -= mod
      p++

    # Write full ints while we can - effectively doing valueSize bits at a time
    if n >= bigValueSize
# Store a local value to work with
      v = if val then @allOnes else `0n`

      loop
        @[p++] = v
        n -= bigValueSize
        break if n < bigValueSize

    # Now do the final partial int, if necessary
    if n
      mod = n & (bigValueSize - `1n`)

      # Calculate the mask
      mask = (`1n` << mod) - `1n`

      if val
        @[p] |= mask
      else
        @[p] &= ~mask
