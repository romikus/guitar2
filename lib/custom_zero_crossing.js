// Generated by CoffeeScript 2.4.1
(function() {
  var ZeroCrossing, decibelToFloat;

  decibelToFloat = function(db) {
    return -Math.pow(10, db / 20);
  };

  module.exports = ZeroCrossing = class ZeroCrossing {
    constructor(lowestFrequency, sampleRate, hysteresisDb) {
      this.sampleRate = sampleRate;
      this.widthMin = (1 / lowestFrequency) * sampleRate;
      this.thresholdMin = 0.5 * decibelToFloat(hysteresisDb);
      this.thresholdMax = -this.thresholdMin;
      this.up = false;
      this.frame = this.edgeFrame = this.min = this.max = 0;
      this.wave = 1;
    }

    push(sample) {
      this.sample = sample;
      //    console.log @sample
      if (this.sample > this.thresholdMax) {
        if (!this.up) {
          this.crossUp();
        }
        this.setMax();
      } else if (this.sample < this.thresholdMin) {
        if (this.up) {
          this.crossDown();
        }
        this.setMin();
      }
      return this.frame++;
    }

    crossUp() {
      return this.up = true;
    }

    crossDown() {
      this.up = false;
      this.processWave();
      this.min = this.max = 0;
      return this.edgeFrame = -1;
    }

    setMax() {
      if (this.max < this.sample) {
        this.max = this.sample;
        return this.edgeFrame = this.frame;
      }
    }

    setMin() {
      if (this.min > this.sample) {
        return this.min = this.sample;
      }
    }

    processWave() {
      if (this.max < this.thresholdMax || this.min > this.thresholdMin) {
        return;
      }
      this.onEdge(this.edgeFrame, this.max);
      return this.wave++;
    }

  };

}).call(this);

//# sourceMappingURL=custom_zero_crossing.js.map
