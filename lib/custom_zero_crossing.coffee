decibelToFloat = (db) ->
  -Math.pow(10, db / 20)

module.exports = class ZeroCrossing
  constructor: (lowestFrequency, sampleRate, hysteresisDb) ->
    @sampleRate = sampleRate
    @widthMin = (1 / lowestFrequency) * sampleRate
    @thresholdMin = 0.5 * decibelToFloat hysteresisDb
    @thresholdMax = -@thresholdMin
    @up = false
    @frame = @edgeFrame = @min = @max = 0
    @wave = 1

  push: (@sample) ->
#    console.log @sample
    if @sample > @thresholdMax
      if not @up
        @crossUp()
      @setMax()
    else if @sample < @thresholdMin
      if @up
        @crossDown()
      @setMin()

    @frame++

  crossUp: ->
    @up = true

  crossDown: ->
    @up = false
    @processWave()
    @min = @max = 0
    @edgeFrame = -1

  setMax: ->
    if @max < @sample
      @max = @sample
      @edgeFrame = @frame

  setMin: ->
    @min = @sample if @min > @sample

  processWave: ->
    return if @max < @thresholdMax or @min > @thresholdMin

    @onEdge @edgeFrame, @max

    @wave++
