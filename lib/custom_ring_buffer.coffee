module.exports = class RingBuffer extends Array # Float32Array
  constructor: (len) ->
    --len
    len |= len >> 1
    len |= len >> 2
    len |= len >> 4
    len |= len >> 8
    len |= len >> 16
    super len + 1
    @mask = len
    @pos = 0
    @clear()

  push: (val) ->
    @pos--
    @pos &= @mask
    @[@pos] = val

  first: ->
    @get 0

  last: ->
    @get @mask

  get: (index) ->
    @[(@pos + index) & @mask]

  set: (index, value) ->
    @[(@pos + index) & @mask] = value

  clear: ->
    @fill null

  shift: ->
    @pos++
