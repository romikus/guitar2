module.exports = class RingBuffer
  constructor: (@size) ->
    @pos = 0
    --@size
    @size |= @size >> 1
    @size |= @size >> 2
    @size |= @size >> 4
    @size |= @size >> 8
    @size |= @size >> 16
    @mask = @size
    @data = new Array ++@size
    @clear()

  push: (val) ->
    @pos--
    @pos &= @mask
    @data[@pos] = val

  front: (val) ->
    @get 0

  back: (val) ->
    @get @mask

  get: (index) ->
    @data[(@pos + index) & @mask]

  clear: ->
    @data.fill 0

  pop_front: ->
    @pos++
