Bitset = require './bitset'
ZeroCrossing = require './zero_crossing'
BitstreamAcf = require './bitstream_acf'

class Collector
  constructor: (@zc, @periodicity_diff_threshold) ->
    @fundamental =
      i1: -1
      i2: -1
      period: -1
      periodicity: 0

    @harmonic_threshold =
      PeriodDetector::harmonic_periodicity_factor * 2 / @zc.windowSize

  save: (incoming) ->
    @fundamental = incoming
    @fundamental.harmonic = 1

  try_harmonic: (harmonic, incoming) ->
    incoming_period = incoming.period / harmonic
    current_period = @fundamental.period
    diff = Math.abs(incoming_period - current_period)
    if diff < @periodicity_diff_threshold
# If incoming is a different harmonic and has better
# periodicity ...
      if incoming.periodicity > @fundamental.periodicity and
          harmonic isnt @fundamental.harmonic
        res = Math.abs incoming.periodicity - @fundamental.periodicity
        periodicity_diff = res

        # If incoming periodicity is within the harmonic
        # periodicity threshold, then replace @fundamental with
        # incoming. Take note of the harmonic for later.
        if periodicity_diff < @harmonic_threshold
          @fundamental.i1 = incoming.i1
          @fundamental.i2 = incoming.i2
          @fundamental.periodicity = incoming.periodicity
          @fundamental.harmonic = harmonic
        else # else we save incoming (replacing current @fundamental).
          @save incoming

      true

    false

  process_harmonics: (incoming) ->
    @try_harmonic(5, incoming) or
      @try_harmonic(4, incoming) or
      @try_harmonic(3, incoming) or
      @try_harmonic(2, incoming) or
      @try_harmonic(1, incoming)

  add: (incoming) ->
    if @fundamental.period is -1 or
      not @process_harmonics(incoming) and
        incoming.periodicity > @fundamental.periodicity
      @save incoming

  get: (info, result) ->
    if info.period isnt -1
      first = @zc.get info.i1
      next = @zc.get info.i2
      result.period = first.fractionalPeriod(next) / info.harmonic
      result.periodicity = info.periodicity
    else
      result.period = -1
      result.periodicity = 0

module.exports = class PeriodDetector
  pulse_threshold: 0.6
  harmonic_periodicity_factor: 15
  periodicity_diff_factor: 0.008

  constructor: (lowest_freq, highest_freq, sps, hysteresis) ->
    @zc = new ZeroCrossing hysteresis, ~~(1 / lowest_freq * 2 * sps)
    @min_period = ~~(1 / highest_freq * sps)
    @bits = new Bitset @zc.windowSize
    @weight =  2 / @zc.windowSize
    @mid_point = @zc.windowSize / 2
    @periodicity_diff_threshold = @mid_point * @periodicity_diff_factor
    @predicted_period = -1
    @edge_mark = 0
    @predict_edge = 0
    @fundamental = period: -1, periodicity: 0

  setBitstream: ->
# console.log "set bitstream"
    threshold = @zc.peakPulse() * @pulse_threshold

    @bits.clear()
    i = 0
    # console.log "before"
    # console.log "0: #{@bits[0]}"
    # console.log "1: #{@bits[1]}"
    # console.log "2: #{@bits[2]}"
    # console.log "3: #{@bits[3]}"
    # console.log "4: #{@bits[4]}"
    # console.log "5: #{@bits[5]}"
    # console.log "6: #{@bits[6]}"
    # console.log "7: #{@bits[7]}"
    # console.log "8: #{@bits[8]}"
    # console.log "9: #{@bits[9]}"
    while i isnt @zc.num_edges
      info = @zc.get(i)
      if info.peak >= threshold
        pos = Math.max info.leading_edge, 0
        n = info.trailing_edge - pos
        # console.log "#{pos}, #{n}"
        @bits.set3 pos, n, 1
      i++
# console.log "after"
# console.log "0: #{@bits[0]}"
# console.log "1: #{@bits[1]}"
# console.log "2: #{@bits[2]}"
# console.log "3: #{@bits[3]}"
# console.log "4: #{@bits[4]}"
# console.log "5: #{@bits[5]}"
# console.log "6: #{@bits[6]}"
# console.log "7: #{@bits[7]}"
# console.log "8: #{@bits[8]}"
# console.log "9: #{@bits[9]}"

  autocorrelate: ->
    threshold = @zc.peakPulse() * @pulse_threshold
    # console.log "zc.peak: #{@zc.peak}"
    # console.log "zc.peak_update: #{@zc.peak_update}"
    # console.log "pulse_threshold: #{@pulse_threshold}"

    ac = new BitstreamAcf @bits
    collect = new Collector @zc, @periodicity_diff_threshold
    @autocorrelationLoop threshold, ac, collect
    # console.log collect.fundamental
    collect.get collect.fundamental, @fundamental

  autocorrelationLoop: (threshold, ac, collect) ->
    for i in [0...@zc.num_edges - 1] by 1
      first = @zc.get i
      # console.log "i: #{i}"
      # console.log "first.peak: #{first.peak}"
      # console.log "threshold: #{threshold}"
      if first.peak >= threshold
        for j in [i + 1...@zc.num_edges] by 1
          next = @zc.get j
          # console.log "j: #{j}"
          # console.log "next.peak: #{next.peak}"
          # console.log "threshold: #{threshold}"
          if next.peak >= threshold
            period = first.period next
            if period > @mid_point
              break

            if period >= @min_period
# console.log "period: #{period}"
              count = ac.getCount period
              periodicity = 1 - count * @weight
              # console.log "count: #{count}"
              # console.log "weight: #{@weight}"
              # console.log "collect(#{i}, #{j}, #{~~period}, #{periodicity})"
              collect.add i1: i, i2: j, period: ~~period, periodicity: periodicity
              if count is 0
                return # Return early if we have perfect correlation

  detect: (s) ->
# Zero crossing
    prev = @zc.State
    zc = @zc.value s

    if not zc and prev isnt zc
      @edge_mark++
      @predicted_period = -1

    if @zc.is_reset()
      @fundamental = period: -1, periodicity: 0

    # console.log "prev: ", prev
    # console.log "zc: ", zc
    # console.log "_edge_mark: ", @edge_mark
    # console.log "_zc.is_reset(): ", @zc.is_reset()
    # console.log "_zc.is_ready(): ", @zc.ready

    if @zc.ready
      @setBitstream()
      @autocorrelate()
      return true

    false

  harmonic: (index) ->
    if index > 0
      if index is 1
        return @fundamental.periodicity

      target_period = @fundamental.period / index
      if target_period >= @min_period and target_period < @mid_point
        ac = new BitstreamAcf @bits
        # console.log "target_period: #{target_period}"
        count = ac.getCount Math.round target_period
        # console.log "harmonic count: #{count}"
        # process.exit()
        periodicity = 1 - count * @weight
        return periodicity

    0

  state: ->
    @zc.State

  predict_period: ->
    if @predicted_period is -1 and @edge_mark isnt @predict_edge
      @predict_edge = @edge_mark
      if @zc.num_edges > 1
        threshold = @zc.peakPulse() * @pulse_threshold
        for i in [@zc.num_edges - 1...0] by -1
          edge2 = @zc.get i
          if edge2.peak >= threshold
            for j in [i - 1..0] by -1
              edge1 = @zc.get j
              if edge1.similar edge2
                @predicted_period = edge1.fractionalPeriod edge2
                return @predicted_period

    @predicted_period

  is_ready: ->
    @zc.ready
