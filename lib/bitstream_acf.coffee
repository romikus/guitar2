Bitset = require './bitset'
valueSize = Bitset::valueSize
int64 = new BigUint64Array 1

module.exports = class BitstreamAcf
  constructor: (@bits) ->
# console.log "0: #{@bits[0]}"
# console.log "1: #{@bits[1]}"
# console.log "2: #{@bits[2]}"
# console.log "3: #{@bits[3]}"
# console.log "4: #{@bits[4]}"
# console.log "5: #{@bits[5]}"
# console.log "6: #{@bits[6]}"
# console.log "7: #{@bits[7]}"
# console.log "8: #{@bits[8]}"
# console.log "9: #{@bits[9]}"
    @midArray = ~~(@bits.size / valueSize / 2 - 1)

  getCount: (pos) ->
# console.log "pos: #{pos}"
# console.log "valueSize: #{valueSize}"
# console.log "midArray: #{@midArray}"
    p1 = 0
    p2 = ~~(pos / valueSize)
    shift = pos % valueSize
    count = 0

    # console.log "index: #{p2}"
    # console.log "shift: #{shift}"

    if shift is 0
      while p1 isnt @midArray
        count += @countBits @bits[p1++] ^ @bits[p2++]
    else
      shift2 = BigInt valueSize - shift
      # console.log "shift2: #{shift2}"
      # console.log "p1: #{p1}"
      # console.log "p2: #{p2}"
      while p1 isnt @midArray
        v = @bits[p2++] >> BigInt(shift)
        # console.log "v: #{v}"
        # console.log "p2: #{@bits[p2]}"
        # console.log "shift2: #{shift2}"
        int64[0] = @bits[p2] << shift2
        v |= int64[0]
        # console.log "v|: #{v}"
        # console.log "^: #{@bits[p1] ^ v}"
        # console.log "count bits: #{@countBits @bits[p1] ^ v}"
        count += @countBits @bits[p1++] ^ v

    count

  countBits: (i) ->
    count = 0
    for char in i.toString 2
      count++ if char is '1'
    count
